<?php
// @codeCoverageIgnoreStart
//OLD Way
function source_auto_loader($class)
{
    $parts = explode('\\', $class);
    $folder = dirname(__FILE__);
    foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder)) as $entry){
        if(file_exists($entry.'/' . end($parts) . '.php')){
            require_once $entry.'/' . end($parts) . '.php';
        }
    }
}

function source_namespace_auto_loader($class){
    $parts = explode('\\', $class);
    $slice = array_map('strtolower',array_slice($parts,0,-1));
    require (!empty($slice)? (implode(DIRECTORY_SEPARATOR,$slice).DIRECTORY_SEPARATOR) :"") .end($parts). '.php';
}
// @codeCoverageIgnoreEnd

spl_autoload_register('source_auto_loader');
//spl_autoload_register('source_namespace_auto_loader');

?>