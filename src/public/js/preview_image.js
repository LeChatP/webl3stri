function preview(input) {
    if(input.files && input.files[0]){
        var obj = new FileReader();
        obj.onload = function( data){
            var img = document.getElementById("thumbnail");
            img.src = data.target.result;
            img.style.display = "block";
            img.style.objectFit = "contain"
            img.style.width = 48;
            img.style.height = 48;
        }
        obj.readAsDataURL(input.files[0]);
    }

}

