<?php
/**
 * Classe Gérant les fonctions de sécurité du site internet comme la protection du mot de passe, mais aussi contre le vol de session PHP
 */
  
class Security{

    /**
     * Crée l'empreinte du mot de passe donné.
     */
    public static function protectPass(string $pass):string{
        return password_hash($pass, PASSWORD_DEFAULT);
    }

    /**
     * Check if the password matches the user's stored password
     * @return bool True if the passwords matches
     */
    public static function checkPassword(string $clear,string $stored) :bool {
        return password_verify($clear, $stored);
    }
    /**
     * Start and check the session of user
     */
    public static function checkSession(){
        self::checkSessionCreated();
        $remember = null;

        //on protège contre le vol de données
        if(!isset($_SESSION['USERAGENT'])) $_SESSION['USERAGENT'] = $_SERVER['HTTP_USER_AGENT'];
        if($_SESSION['USERAGENT'] !== $_SERVER['HTTP_USER_AGENT']){
            self::closeSession();
            return;
        }
        //protection contre le session fixation hijack
        if (!isset($_SESSION['CREATED'])) {
            $_SESSION['CREATED'] = time();
        } else if (time() - $_SESSION['CREATED'] > 1800) {
            session_regenerate_id(true);
            $_SESSION['CREATED'] = time();
        }
    }
    /**
     * Ferme la session si existante
     */
    public static function closeSession(){
        self::checkSessionCreated();
        if(session_status() ===PHP_SESSION_ACTIVE){
            session_unset();
            session_destroy();
        }
    }
    /**
     * Vérifie que la session PHP est active
     */
    public static function checkSessionCreated(){
        if(session_status() === PHP_SESSION_NONE){
            session_start();
        }
    }
}

?>
