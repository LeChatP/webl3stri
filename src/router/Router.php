<?php
namespace Router;
/**
 * Le routeur sers à lister les chemins disponibles à la route
 */
class Router {

    private $url;
    private $get = []; //contient les routes du site concernant les requêtes get
    private $post = [];  //contient les routes du site concernant les requêtes post
    private $page404;
    private $page403;

    private $route;

    public function __construct(string $url, callable $page404, callable $page403){
        $this->url = $url;
        $this->set404($page404);
        $this->set403($page403);
    }

    /**
     * Enregistre un chemin d'une requête de type GET
     * @param path : le chemin a enregistrer
     * @param page : la page a executer
     * @param regex : un tableau contant tous les paramètres éventuels, si la page est la page du profile de l'utilisateur, le chemin devra comporter son identifiant par exemple.
     */
    public function get(string $path,string $name,callable $page,array $regex=[],AccessLevel $level = null){
        $this->get[$name] = new Route($path,$page,$regex,$level);
    }

    /**
     * Enregistre un chemin d'une requête de type POST
     * @param path : le chemin a enregistrer
     * @param page : la page a executer
     * @param regex : : un tableau contant tous les paramètres éventuels, si la page est la page du profile de l'utilisateur, le chemin devra comporter son identifiant par exemple. Il est donc nécéssaire de donner un tableau avec ['id' => '[0-9]+']
     */
    public function post(string $path,string $name,callable $page,array $regex=[],AccessLevel $level = null){
        $this->post[$name] = new Route($path,$page,$regex,$level);
    }

    /**
     * Enregistre la fonction en cas de chemin inconnu
     */
    public function set404(callable $page){
        $this->page404 = new Route("",$page);
    }

    /**
     * Enregistre la fonction en cas de chemin non autorisé
     */
    public function set403(callable $page){
        $this->page403 = new Route("",$page);
    }

    /**
     * récupère la fonction associée a la route
     */
    public function getCallable(AccessLevel $level){
        $callable = $this->getRoute()->getCallable($level);
        return $callable !== false ? $callable : $this->page403->getCallable($level);
    }

    /**
     * récupère les paramètres dans l'url
     */
    public function getParams(AccessLevel $level){
        $params = $this->getRoute()->getMatches($level);
        return $params !== false ? $params : $this->page403->getMatches($level);
    }

    private function getRoute() : Route{
        if($this->route == null){//routing only one time
            $routing = strtolower($_SERVER['REQUEST_METHOD']);
            foreach($this->$routing as $route){
                if($route->match($this->url)){
                    $this->route = $route;
                    return $this->route;
                }
            }
            return $this->page404;
        }else
        {
            return $this->route;
        }
    }

    public function url_is_accessed_by(string $type,string $name,AccessLevel $access) : bool{
        $type = strtolower($type);
        if(isset($this->$type[$name])){
            return $this->$type[$name]->is_accessed_by($access);
        }
        return false;
    }

    public function url(string $type,string $name,array $params=[]):string{
        $type = strtolower($type);
        if(isset($this->$type[$name])){
            return $this->$type[$name]->getUrl($params);
        }
        trigger_error("No route found",E_USER_WARNING);
    } //@codeCoverageIgnore

    public function urlGET(string $name,array $params=[]):string{
        return $this->url("GET",$name,$params);
    }

    public function urlPOST(string $name,array $params=[]):string{
        return $this->url("POST",$name,$params);
    }
}

?>