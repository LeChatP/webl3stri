<?php
namespace Router;
/**
 * Classe définissant les niveaux d'accès sur l'ensemble du site internet. Il indique si un utilisateur a le droit d'accéder à une page ou un contenu.
 */
class AccessLevel {
    const VISITOR = 0;
    const MEMBER = 1;
    const OWNER = 2;
    const ADMIN = 3;

    /**
     * @var int niveau d'accès
     */
    private $level;
    /**
     * @var int identifiant du propriétaire
     */
    private $owner;

    /**
     * Constructeur pour AccessLevel
     * défini le niveau d'accès pour le contenu
     * @param AccessLevel niveau d'accès nécéssaire pour accéder au contenu
     * @param owner si le contenu necéssite d'être propriétaire du document
     */
    public function __construct(int $access = AccessLevel::VISITOR,int $owner = 0){
        $this->owner = $owner;
        $this->level = $access;
    }

    public function __destruct()
    {
        unset($this->owner);
        unset($this->level);
    }

    /**
     * Vérifie si un niveau d'accès est autorisé pour l'utilisateur $level
     * @param level niveau d'accès de l'utilisateur
     * @return bool si l'utilisateur a l'accès requis
     */
    public function is_accessed_by(AccessLevel $level) :bool{
        if($this->is_owner($level->getUser())){
            return $level->getLevel() == AccessLevel::MEMBER || AccessLevel::is_admin($level->getLevel()); //l'utilisateur ne peut pas avoir le niveau de propriétaire
        }
        else
            return $level->getLevel() >= $this->level;
    }

    /**
     * Vérifie si un utilisateur possède l'accès pour le $level donné en paramètre
     */
    public function has_access(AccessLevel $level):bool{
        return $level->is_accessed_by($this);
    }

    /**
     * Vérifie que l'utilisateur est propriétaire du contenu.
     * @return bool vrai si l'utilisateur est propriétaire
     */
    public function is_owner(int $user = 0):bool{
        return $user != 0 && $user == $this->owner;
    }

    /**
     * Donne le niveau nécéssaire pour accéder au contenu
     * @return int niveau d'accès du contenu 
     */
    public function getLevel() :int{
        return $this->level;
    }

    public function getUser(): int{
        return $this->owner;
    }

    /**
     * Vérifie que l'utilisateur est Administateur
     * @return bool vrai si l'utilisateur est administrateur
     */
    public static function is_admin(int $access = AccessLevel::VISITOR):bool{
        return $access == AccessLevel::ADMIN;
    }
}

?>