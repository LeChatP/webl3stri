<?php
namespace Router;
/**
 * Une Route est un chemin sur le site internet c'est à dire : 
 * http://mondomaine/le-chemin
 * le-chemin est le chemin d'une route (si existante)
 * Cette classe permet de lier un chemin sur le site internet à une fonction.
 * Dans le cadre du Modèle Vue Controlleur, la fonction à executer sera un contrôleur qui effectuera les actions lié au chemin et donc la demande.
 */
class Route{

    /**
     * @var string chemin de l'URL qui mène à la fonction
     */
    private $path;
    /**
     * @var callable tableau de type callable, c'est une fonction à executer
     */
    private $callable;
    /**
     * @var array liste des paramètres réels du chemin
     */
    private $matches;
    /**
     * @var array liste des paramètres pris en charge par la route
     */
    private $params;
    /**
     * @var AccessLevel niveau d'accès nécéssaire pour la route
     */
    private $access;
    
    /**
     * Constructeur
     * Une route peut avoir un paramètre càd : https://exemple/explore/15
     * la route pour ce chemin sera 'explore/:id'
     * si le perametre est en conflit avec le chemin exemple : https://exemple/explore/super-jeu-15
     * la route pour ce chemin sera 'explore/:title-:id' mais les paramètres seront vu comme 'super' et 'jeu-15'
     * le paramètre paramsRegex permet de définir les match des chemin on pourra alors dire : {"id" => "[0-9]+"} ce qui corrige le problème
     * @param path Le chemin pour accéder à la page
     * @param callable La fonction liée au chemin
     * @param paramsRegex expression régilière qui vérifiera l'intégrité du paramètre
     */
    public function __construct(string $path, callable $callable, array $paramsRegex = [], AccessLevel $access = null){
        $this->path = trim($path,'/');
        $this->callable = $callable;
        $this->params = $paramsRegex;
        if ($access != null) $this->access = $access;
        else $this->access = new AccessLevel();
    }

    /**
     * vérifie que le chemin donné en paramètre correspond à l'expression donné lors de sa création
     * @param url le chemin a tester
     * @return bool si la Route correspond à la requete de l'utilisateur
     */
    public function match(string $url){
        $url = trim($url,'/');
        $url = rtrim($url,'?'); // on retire le ? de tout formulaire
        $path = preg_replace_callback('#:([\w]+)#',[$this,'paramMatch'],$this->path); // on transforme les paramètres en expressions régulière (ne comprenant pas de /)
        $regex = "#^$path$#i"; // on place le tout en tant qu'expression réggulière
        if(!preg_match($regex,$url,$matches)){ // si l'expression correspond au parametre $path alors tout est enregistré dans $matches
            return false;
        }
        array_shift($matches); // on retire le premier paramètre qui est l'ensemble du match, pour ne laisser que les paramètres GET précisés dans $path
        $this->matches = $matches; //on enregistre les paramètres afin de les envoyer dans la page lors du call
        return true;
    }

    private function paramMatch($match){
        if(isset($this->params[$match[1]])){
            $res= str_replace('(','(?:',$this->params[$match[1]]); //on retire le match pour un eventuel groupement
            return '('.$res.')'; //on retourne le match du regex paramétré
        }
        return '([^/]+)'; //on retourne le regex de base 
    }

    /**
     * récupère la fonction à executer une fois que le match du chemin est ok
     */
    public function getCallable(AccessLevel $level){
        return $this->is_accessed_by($level) ? $this->callable : false;
    }

    /**
     * On récupère les arguments obtenus si existants
     */
    public function getMatches(AccessLevel $level){
        return $this->is_accessed_by($level) ? $this->matches : false;
    }

    /**
     * Vérifie que l'utilisateur possède l'accès nécessaire à la route
     */
    public function is_accessed_by(AccessLevel $level):bool{
        return $this->access->is_accessed_by($level);
    }

    /**
     * Cette fonction sers à obtenir l'url de la route
     */
    public function getUrl(array $params):string{
        $path = $this->path;
        foreach($params as $k => $v){
            $path = str_replace(":$k",$v,$path);
        }
        return '/'.$path;
    }
}

?>
