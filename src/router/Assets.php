<?php
namespace Router;
use InvalidArgumentException;
use Exceptions\FileNotFoundException;
/**
 * Classe qui retrouve les chemins des fichiers ressources
 */
class Assets{

    public const TYPES = [
        "images"=>["png","gif","jpg","jpeg","bnp","svg"],
        "css"=>["css"],
        "js"=>["js"],
        ""=>["ico"]
    ];

    public function __construct(){
    }


    public function getAsset(string $asset):string{
        if(strlen($asset) == 0) throw new InvalidArgumentException("please give correct filename");
        $ext = pathinfo($asset, PATHINFO_EXTENSION);
        foreach(Assets::TYPES as $key => $array){
            if(in_array($ext,$array)){
                if ($key ==="" && file_exists(__DIR__."/../public/".$asset)){
                    error_log("/$key$asset");
                    return "/".$key.$asset;
                }elseif(file_exists(__DIR__."/../public/".$key."/".$asset)){
                    error_log("$key/$asset");
                    return "/".$key."/".$asset;
                }else {
                    throw new FileNotFoundException("The asset ".$asset." does not exist",__DIR__."../public/".$key."/".$asset);
                }
            }
        }
        throw new FileNotFoundException("The asset ".$asset." does not exist",__DIR__."../public/".$key."/".$asset);
    }
}
?>