<?php
namespace Router;
/**
 * Le Renderer permet de rediriger a partir d'un nom, la vue correspondante,
 * il lui transmettra les objets communs tel que le routeur pour la création d'URL
 * ou l'objet des Assets pour les URL des images
 * ou du niveau d'accès de l'utilisateur
 */
class Renderer{

    private $router;
    private $assets;
    private $access;
    private $params;
    private $path;

    public function __construct(string $viewspath, Router $router, Assets $assets,AccessLevel $access,array $params){
        $this->path = $viewspath;
        $this->router = $router;
        $this->params = $params;
        $this->access = $access;
        $this->assets = $assets;
    }

    /**
     * Cette fonction va appeller la vue viewname et transférer les variables contenues dans la map params
     */
    public function render(string $viewname, array $params = []):string{
        $path = __DIR__."/../".$this->path.str_replace(".","/",$viewname) .'.php';
        if(!file_exists($path))return "";
        ob_start();
        $router = $this->router;
        $assets = $this->assets;
        $access = $this->access;
        $renderer = $this;
        if(!is_null($this->params))extract($this->params);
        if(!is_null($params))extract($params);
        require($path);
        return ob_get_clean();
    }

    public function addParam($key,$value){
        $this->params[$key] = $value;
    }
}
?>