<?php
namespace Exceptions;
/**
 * Exception levée lorsqu'un fichier n'est pas trouvé
 */
class FileNotFoundException extends \Exception
{
  protected $timestamp;
  protected $filename;

  public function __construct($message=NULL, $filename=NULL, $code=0)
  {  
    if($message == NULL){
        $message = "Le fichier ".$filename." est inexistant";
    }
    parent::__construct($message, $code);
    $this->timestamp = time();
    $this->filename = $filename;
  }

  public function getTimestamp() {
    return $this->timestamp;
  }

  public function getFilename(){
      return $this->filename;
  }
}

?>