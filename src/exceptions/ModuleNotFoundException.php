<?php
namespace Exceptions;

/**
 * Exception levée lorsqu'un module du namespace \Modules\ n'est pas trouvé
 */
class ModuleNotFoundException extends \Exception
{
  protected $modulename;

  public function __construct($message=NULL, $modulename=NULL, $code=0)
  {  
    if($message == NULL){
        $message = "Le module ".$modulename." est inexistant";
    }
    parent::__construct($message, $code);
    $this->modulename = $modulename;
  }

  public function getModulename(){
      return $this->modulename;
  }
}

?>