
<?php
/**
 * Classe permettant de génerer un tableau HTML simplement.
 */
class Tableau
{
  /** 
   * @var bool
   */
  private $hasLineHeaders;
  /** 
   * @var array
   */
  private $colHeaders;
  /**
   * @var array
   */
  private $linesContent;
  /**
   * @var array
   */
  private $classTable;

  /**
   * @var int
   */
  private $maxcols;

  function __construct()
  {
    $this->linesContent = array();
    $this->colHeaders = [];
    $this->classTable = ["table"=>null];
    $this->maxcols = 0;
  }

  function setColumnHeader($entetes)
  {
    $this->colHeaders = $entetes;
  }

  function setLineHeader()
  {
    $this->hasLineHeaders = true;
  }

  function setLine(array $ligne)
  {
    $count = count($ligne);
    if($count > $this->maxcols){
      $this->maxcols = $count;
    }
    array_push($this->linesContent, $ligne);
  }

  function addTableCss($class){
    $this->classTable[$class] = null;
  }

  function setTableCss($list){
    $this->classTable = array_fill_keys(array_merge($list,array_keys($this->classTable)),null);
  }

  function getTableau()
  {
    $tab = '<table class="'.implode(' ',array_keys($this->classTable)).'">';
    if(!empty($this->colHeaders)) {
      $tab_Header = '<thead>';
      if($this->hasLineHeaders && count($this->colHeaders)+1 == count($this->linesContent[0])) {
        $tab_Header = $tab_Header.'<th scope="col">&nbsp;</th>';
      }
      foreach ($this->colHeaders as $valeur) {
        $tab_Header = $tab_Header.'<th scope="col">'.$valeur.'</th>';
      }
      $tab_Header = $tab_Header.'</thead>';
      $tab = $tab.$tab_Header;
    }

    $tab_Col = '<tbody>';
    foreach ($this->linesContent as $cle => $lineValue) {
      $tab_Col = $tab_Col.'<tr class="row">';
      if($this->hasLineHeaders){
        $tab_Col = $tab_Col.'<th scope="row">'.$lineValue[0].'</th>';
        array_shift($lineValue);
      }
      foreach ($lineValue as $val) {
        $tab_Col = $tab_Col.'<td>'.$val.'</td>';
      }
      //on remplit les colonnes si il y en a manquantes dans la ligne
      if(count($lineValue) < $this->maxcols) preg_replace('/<t([dh]( scope="row")*)>(.*)$/','<t$1 colspan="'.count($this->colHeaders)-count($lineValue).'">$3',$tab_Col);
      $tab_Col = $tab_Col.'</tr>';
    }
    $tab_Col = $tab_Col.'</tbody>';

    $tab = $tab.$tab_Col;
    $tab = $tab.'</table>';

    return $tab;
  }

}

?>
