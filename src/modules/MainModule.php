<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
use Router\Assets;
use Models\UserService;
use Security;
/**
 * Module principal, il gère l'authentification et les pages principales du site internet
 */
class MainModule extends Module {
    public function __construct(Router $router, Renderer $renderer, AccessLevel $user){
        parent::__construct($router,$renderer,$user,new AccessLevel());
        $this->get("/","home","home");
        $this->get("/register","register","registerGET");
        $this->post("/register","register","registerPOST");
        $this->get("/login","login","loginGET");
        $this->post("/login","login","loginPOST");
        $this->get("/history","history","historyGET");
        $this->get("/disconnect","logout","logoutPOST");
    }

    public function home(){
        echo $this->renderer->render("home");
        return 200;
    }

    public function registerGET(){
        echo $this->renderer->render("register",["isknow"=>false, "errInput"=>false]);
        return 200;
    }
    
    public function registerPOST(){
        if(!empty($_POST['annuler'])){
            header('Location: '. $this->router->urlGET(("home")));
            return 301;
        }
       
        $id = UserService::getId($_POST['username']);
        $isknow = !empty($id);
        $valideUser = isset($_POST['username']) && strlen(trim($_POST['username'])) > 0 ;
        $validePass = isset($_POST['password']) && strlen(trim($_POST['password'])) > 0;
        $errInput = !($valideUser && $validePass);
        $error = null;
        if(!$isknow && !$errInput){
            $user = new UserService(0);
            $avatarPath = null;
            if (isset($_FILES['avatar'])){
                if($_FILES['avatar']['error'] !== UPLOAD_ERR_NO_FILE) {
                    $avatarExtension = strtolower(substr(strrchr($_FILES['avatar']['name'], '.'), 1));
                    list($width, $height, $type, $attr) = getimagesize("geeks.png");
                    if ($_FILES['avatar']['error'] > 0) {
                        if ($_FILES['avatar']['error'] === UPLOAD_ERR_INI_SIZE || $_FILES['avatar']['error'] === UPLOAD_ERR_FORM_SIZE) {
                            $error = 'Taille trop grande';
                        } elseif ($_FILES['avatar']['error'] === UPLOAD_ERR_PARTIAL) {
                            $error = 'Fichier transféré partiellement';
                        } else {
                            $error = 'Inconnue...';
                        }
        
                        $error = 'Erreur lors du transfert : ' . $error;
                    } elseif (!in_array($avatarExtension, Assets::TYPES['images'])) {
                        $error .= 'Extension incorrecte';
                    } elseif($width != $height){
                       $error .= "L'image doit être carrée";
                    } else {
                        $avatarPath = 'user/avatar_' . $_POST['username'] . '.' . $avatarExtension;
                        if(!mkdir(__DIR__.'/../public/images/user', 0755, true))
                        if (!move_uploaded_file($_FILES['avatar']['tmp_name'], __DIR__.'/../public/images/' . $avatarPath)) {
                            $error .= 'Erreur lors de la sauvegarde du fichier.';
                            $avatarPath = null;
                        }
                    }
                }
            }
            if($error == null){
                $idUser = $user->createUser($_POST['username'], Security::protectPass($_POST['password']), $avatarPath);
                $_SESSION['idUser'] = $idUser;
                header('Location: '. $this->router->urlGET(("home")));
                return 301;
            }
        }
        echo $this->renderer->render("register", ["isknow"=>$isknow, "errInput"=>$errInput,"uploadError"=>$error]);
        return 200;  
    }

    public function loginGET(){
        echo $this->renderer->render("login",["isknow"=> true]);
        return 200;
    }
    
    public function loginPOST(){
        if(!empty($_POST["annuler"])){
            header('Location: '. $this->router->urlGET(("home")));
            return 301;
        }

       $id = UserService::getId($_POST['username']);
       $isknow = !empty($id);

       if($isknow){
            $userSer = new UserService($id);
            $user = $userSer->getUtilisateur();
            if(Security::checkPassword($_POST['password'],$user->getPassword())){
                $_SESSION['idUser'] = $user->getId();
                header('Location: '. $this->router->urlGET(("home")));
                return 301;
            }
            $isknow = false;
       }
        echo $this->renderer->render("login",["isknow"=>$isknow]);
        return 200;
    }

    public function historyGET(){
        echo $this->renderer->render("history");
        return 200;
    }

    public function logoutPOST(){
        Security::closeSession();
        header('Location: '. $this->router->urlGET(("home")));
        return 301;
    }
}
?>