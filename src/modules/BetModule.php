<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
use Models\MatchService;
use Models\BetService;
use Contents\Match;
/**
 * Module pour les paris, il gère les actions utilisateurs en relation avec les Paris. Les chemins nécessitent par défaut d'être authentifié pour y accéder
 */
class BetModule extends Module {
    public function __construct(Router $router, Renderer $renderer, AccessLevel $user){
        parent::__construct($router,$renderer,$user,new AccessLevel(AccessLevel::MEMBER));
        $this->get("/bet","bet.start","howtoGET",[],new AccessLevel(AccessLevel::VISITOR));
        $this->get("/bet/new","bet.new","newBetGET",[]);
        $this->get("/bet/match/:id","bet.match","newBetMatchGET",["id"=>"[0-9]+"]);
        $this->post("/bet/match/:id","bet.post","newBetMatchPOST",["id"=>"[0-9]+"]);
        $this->get("/mybets","bet.user","myBetGET",[]);
    }

    public function howtoGET(){
        echo $this->renderer->render("bet.how");
        return 200;
    }

    public function myBetGET(){
        echo $this->renderer->render("bet.mybets");
        return 200;
    }

    public function newBetGET(){
        $matchs = new MatchService();
        echo $this->renderer->render("bet.newchooser",["matchincoming"=>$matchs->getUnplayedMatchs()]);
        return 200;
    }

    public function newBetMatchGET($id){
        if(($match = $this->getIdMatchUnplayed(intval($id))) != null){
            echo $this->renderer->render("bet.new",["equipeA"=>$match->__get("equipea"),"equipeB"=>$match->__get("equipeb"),"id"=>$id]);
            return 200;
        }else{
            echo $this->renderer->render("error403");
            return 403;
        }
    }

    public function newBetMatchPOST($id){
        $params = [];
        if(($match = $this->getIdMatchUnplayed(intval($id))) != null){
            $required_fields = array("essaisA", "essaisB", "transA", "transB","penaA","penaB");
            foreach ($required_fields as $field) {
                if (!empty($_POST[$field])) {
                    $params['error'] .= "$field ne peut pas être vide<br/>";
                    $name = $field.'error';
                    $params[$name] = "is-danger";
                }elseif(!is_numeric($_POST[$field])){
                    $params['error'] .= "$field doit être un nombre<br/>";
                    $name = $field.'error';
                    $params[$name] = "is-danger";
                }
            }
            // On vérifie que le pari est coorect
            $validbet = isset($_POST['numEqA']) && is_numeric($_POST['numEqA']) && isset($_POST['numEqB']) && is_numeric($_POST['numEqB']);
            if(empty($params['error']) && $validbet){
                $betService = new BetService();
                $res = $betService->getPariMatchUser($this->user->getUser(),$id);
                if($res == null){
                    if($betService->newPari($id,$this->user->getUser(),
                    $_POST['numEqA'],$_POST['essaisA'],$_POST['transA'],$_POST['penaA'],
                    $_POST['numEqB'],$_POST['essaisB'],$_POST['transB'],$_POST['penaB'])){
                        header("location: ".$this->router->urlGET("bet.user"));
                        return 302;
                    }else{
                        $params['error'] .= "Il est impossible de faire ce pari";
                    }
                }else{
                    $params['error'] .= "Vous avez déjà parié sur ce match, vous ne pouvez pas modifier un pari une fois effectué<br/>";
                }
                $params = array_merge($params,["equipeA"=>$match->__get("equipea"),"equipeB"=>$match->__get("equipeb"),"id"=>$id]);
                echo $this->renderer->render("bet.new",$params);
                return 200;
            }
        }else{
            echo $this->renderer->render("error403");
            return 403;
        }
    }

    private function getIdMatchUnplayed($id): ?Match{
        $matchservice = new MatchService();
        
        $matchs = $matchservice->getUnplayedMatchs();
        foreach($matchs as $match){
            if($match->__get("idmatch") == $id){
                return $match;
            }
        }
        return null;
    }

}
?>