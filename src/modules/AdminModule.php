<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
use Models\MatchService;
/**
 * Module pour les pages Administrateur. Les chemins de ce module necessite d'avoir le niveau d'accès Administrateur
 */
class AdminModule extends Module{
    public function __construct(Router $router, Renderer $renderer, AccessLevel $user){
        parent::__construct($router,$renderer,$user,new AccessLevel(AccessLevel::ADMIN));
        $this->get("/admin","admin.panel","getPanel");
        $this->post("/admin/match/run","admin.match.run","postRunMatch");
        $this->post("/admin/match/result","admin.match.result","postResultMatch");
        $this->get("/admin/match/add","admin.match.add","getAddMatch");
        $this->post("/admin/match/add","admin.match.add","postAddMatch");
        $this->get("/admin/joueur/add","admin.joueur.add","getAddJoueur");
        $this->post("/admin/joueur/add","admin.joueur.add","postAddJoueur");
    }

    /**
     * Affiche le panel administrateur
     */
    public function getPanel(){
        $matchService = new MatchService();
        $incoming = $matchService->getUnplayedMatchs();
        $playing = $matchService->getPlayingMatchs();
        echo $this->renderer->render("admin.panel",["incoming_matchs"=>$incoming,"playing_matchs"=>$playing]);
        return 200;
    }

    /**
     * Affiche la page d'ajout de match
     */
    public function getAddMatch(){
        return 200;
    }

    /**
     * Effectue le traitement d'ajout d'un match
     */
    public function postAddMatch(){
        return 200;
    }

    /**
     * affiche la page pour entrer les résultats
     */
    public function getResultMatch($idMatch){
        $matchService = new MatchService();
        $match = $matchService->getMatch($idMatch);
        echo $this->renderer->render("");
        return 200;
    }

    /**
     * Effectue le traitement de lancement d'un match
     */
    public function postResultMatch($idMatch){
        if(isset($_POST["id"]) && is_int($_POST["id"])){
            $eqA = $_POST["equipeA"];
            $essaiA = $_POST["essaiA"];
            $transA = $_POST["transA"];
            $penaA = $_POST["penaA"];
            $eqB = $_POST["equipeB"];
            $essaiB = $_POST["essaiB"];
            $transB = $_POST["transB"];
            $penaB = $_POST["penaB"];
            $fin = $_POST["fin"];
            $matchService = new MatchService();
            $match = $matchService->getMatch($idMatch);
            $info = "";
            $class = "";
            if(!$match->__get("idEtat") == 3){
                $matchService->setPlayed($idMatch,$eqA,$essaiA,$transA,$penaA,$eqB,$essaiB,$transB,$penaB,$fin);
                $info="Les résultats sont enregistrés";
                $class="is-success";
            }else{
                $info="Impossible d'entrer les résultats d'un match déjà terminé";
                $class="is-danger";
            }
        }
        $this->renderer->addParam("info",$info);
        $this->renderer->addParam("class",$class);
        return $this->getPanel();
    }

    /**
     * Effectue le traitement de lancement d'un match
     */
    public function postRunMatch(){
        if(isset($_POST["id"]) && is_int($_POST["id"])){
            $idMatch = intval($_POST["id"]);
            $matchService = new MatchService();
            $match = $matchService->getMatch($idMatch);
            $info = "";
            $class = "";
            if($match->__get("idEtat") == 1 || $match->__get("idEtat") == 4){
                $matchService->setPlaying($idMatch);
                $info="Le match a démarré désormais en cours";
                $class="is-success";
            }
            else{
                $info="Impossible de mettre un match déjà en cours ou terminé dans cet état";
                $class="is-danger";
            }
        }
        $this->renderer->addParam("info",$info);
        $this->renderer->addParam("class",$class);
        return $this->getPanel();
    }

    /**
     * Affiche la page d'ajout d'un joueur
     */
    public function getAddJoueur(){
        return 200;
    }

    /**
     * Effectue le traitement d'ajout d'un joueur
     */
    public function postAddJoueur(){
        return 200;
    }

}
?>