<?php
namespace Modules;
use Router\Renderer;
use Router\Router;
use Router\AccessLevel;
class TestModule extends Module{
    private $called;

    public function __construct(Router $router,Renderer $renderer,AccessLevel $user){
        parent::__construct($router,$renderer,$user);
        $this->get("/","login","null");
    }

    public function getDir(){
        return __DIR__;
    }

    public function null(){

    }
}
?>