<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
/**
 * Le Module gère les Méthodes GET et POST HTTP, affiche la vue et retourne le code HTTP
 * Cette Classe généralise les Modules.
 */
class Module{
    /**
     * @var Renderer
     */
    protected $renderer;
    /**
     * @var Router
     */
    protected $router;
    /**
     * @var AccessLevel niveau d'accès utilisateur
     */
    protected $user;
    /**
     * @var AccessLevel niveau nécessaire d'accès au module
     */
    protected $access;

    public function __construct(Router $router, Renderer $renderer, AccessLevel $userAccess, $accessModule = null){
        $this->renderer = $renderer;
        $this->router = $router;
        $this->user = $userAccess;
        if ($accessModule == null) $accessModule = new AccessLevel();
        $this->access = $accessModule;
    }

    public function getRenderer():Renderer{
        return $this->renderer;
    }

    public function getRouter():Router{
        return $this->router;
    }

    public function getAccessLevel():AccessLevel{
        return $this->access;
    }

    public function get(string $path,string $name,string $call,array $regex = [], $level = null){
        if($level == null) $level = $this->access;
        $this->router->get($path,$name,[$this,$call],$regex,$level);
    }

    public function post(string $path,string $name,string $call,array $regex = [], $level = null){
        if($level == null) $level = $this->access;
        $this->router->post($path,$name,[$this,$call],$regex,$level);
    }
    /**
     * check if user is logged 
     * @return redirection if not logged
     */
    public function is_logged():bool{
        if($this->user->getLevel() == AccessLevel::VISITOR){
            header("location: ".$this->getRouter()->urlGET("login"));
            return false;
        }
        return true;
    }

    public function is_admin():bool{
        return AccessLevel::is_admin($this->user->getLevel());
    }

}
?>