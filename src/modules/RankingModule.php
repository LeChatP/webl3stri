<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
use Models\RankingService;
/**
 * Module gérant les chemins pour les classements
 */
class RankingModule extends Module {
    public function __construct(Router $router, Renderer $renderer, AccessLevel $user){
        parent::__construct($router,$renderer,$user,new AccessLevel());
        $this->get("/classementEquipes","ranking.teams","rankingTeamsGET",[]);
        $this->get("/bet","ranking.bet","rankingBetGET",[]);
        
    }

    public function rankingTeamsGET(){

        $RankingServ = new RankingService(); 
        $result = $RankingServ->getClassementEquipe() ;
        echo $this->renderer->render("classement_equipe",["classement"=>$result]);
        //TODO: afficher la vue "classement_equipe" avec le classement en paramètre voir TeamModule pour le faire
        return 200;
    }

    public function rankingBetGET(){
        echo $this->renderer->render("howtobet"); //TODO
        return 200;
    }

}
?>