<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
use Models\TeamService;
/**
 * Module gérant les pages des joueurs d'une équipe et les équipes.
 */
class TeamModule extends Module {
    public function __construct(Router $router, Renderer $renderer, AccessLevel $user){
        parent::__construct($router,$renderer,$user,new AccessLevel());
        $this->get("/team/:name","team","teamPresentation",["name"=>"[a-zA-Z0-9-]+"]);
        $this->get("/joueurs/:lettre","listJoueurs","listJoueursGET",["lettre"=>"[A-Z]"]);
    }

    public function teamPresentation(string $name){
        $service = new TeamService();
        $equipe = $service->getEquipe($name);
        echo $this->renderer->render("equipe",["name"=>$equipe->__get("pays")]); // nom de la vue + tableau contenant nom de variable=>valeur
        return 200;
    }

    public function listJoueursGET($lettre){
        return 200;
    }

}
?>