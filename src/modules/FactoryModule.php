<?php
namespace Modules;
use Router\AccessLevel;
use Router\Renderer;
use Router\Router;
use InvalidArgumentException;
use Exceptions\ModuleNotFoundException;
/**
 * Classe permettant d'instancier les modules par leur nom, sans spécifier les paramètres nécéssaire à leur instanciation.
 * Application du Patron de conception Factory (Usine)
 */
class FactoryModule{
    private $router;
    private $renderer;
    private $user;
    public function __construct(Router $router, Renderer $renderer,AccessLevel $userAccess){
        $this->router = $router;
        $this->renderer = $renderer;
        $this->user = $userAccess;
    }

    public function getModule(string $name){
        if ($name == "")throw new InvalidArgumentException("You must specify a module name");
        $class = 'Modules\\'.ucfirst($name).'Module';
        if(is_subclass_of($class,"Modules\Module"))
            return new $class($this->router,$this->renderer,$this->user);
        throw new ModuleNotFoundException(null,$class);
    }
}
?>