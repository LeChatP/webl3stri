<?php
namespace Database;
use PDO,Exception;
/**
* Gestionnaire de base de données
* PHP 7 Required
*/
class Database
{
	private $db_url;
	private $db_user;
	private $db_pass;
	private $db_name;
	/**
	 * @var PDO
	 */
	private $pdo;
	private $transaction;

	public function __construct(string $db_url, string $db_user, string $db_pass,string $db_name)
	{
		$this->db_url = $db_url;
		$this->db_user = $db_user;
		$this->db_pass = $db_pass;
		$this->db_name = $db_name;
	}

	//this function must be private, otherwhise this class is useless
	private function getPDO(): PDO{
		if($this->pdo == null){
            $this->pdo = new PDO("pgsql:host=".$this->db_url.";dbname=".$this->db_name,$this->db_user,$this->db_pass);
			// TODO: PDO::ERRMODE_SILENT
			$this->pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
			$this->pdo->query("SET NAMES 'utf8'");
		}
		return $this->pdo;
    }

    public function beginTransaction() : bool {
        $this->transaction = true;
        return $this->getPDO()->beginTransaction();
	}
	/**
	 * Valide la modification si effectuée
	 * retourne false si une erreur survient
	 */
	public function commit(): bool{
		if($this->transaction)
        {
            $this->transaction = false;
            return $this->getPDO()->commit();
        }
		else return false;
	}
	/**
	 * Annule une modification
	 * retourne false si une erreur survient
	 */
	public function rollback(): bool{
		if($this->transaction) {
            $this->transaction = false;
            return $this->getPDO()->rollBack();
        }
		else return false;
	}
	/**
	 * Ferme la connection courante
	 */
	public function close(): void{
        $this->rollback();
        $this->pdo = null;
	}

	/**
	 * Execute tout un fichier
	 */
	public function executeFile($file){
		return $this->getPDO()->exec(file_get_contents($file))==0;
	}

	/**
	 * Prépare et exécute une requete de recherche
	 * permets une lecture complète ou séquentielle via objets de manière sécurisée
	 * si un seul argument donné alors un tableau de tableau sera renvoyé
	 * Exemple : $result = $this->query("SELECT * From Joueur")
	 * donne un array : 
	 * [
	 *  ["nom"=>"NOMJOUEUR","prenom"=>"PRENOMJOUEUR"],
	 *  ["nom"=>"NOMJOUEUR2","prenom"=>"PRENOMJOUEUR2"]
	 * ]
	 * donc pour accéder au prénom du deuxième joueur il faut : 
	 * $result[2]["prenom"]
	 * @return PDOStatement|array si fetchAll = false sinon renvoi une liste d'objet
	 */
	public function query(string $statement, array $attribus = null, string $className = null, bool $fetchall = true){
		if($this->isNotSelect($statement)) throw new Exception("cannot exec other statement than SELECT in this function, please use exec()");
		$req=$this->getPDO()->prepare($statement);
		if($attribus !== null) $req->execute($attribus);
		else $req->execute();
		if($className!==null)
			$req->setFetchMode(PDO::FETCH_CLASS,"Contents\\".$className);
		else
			$req->setFetchMode(PDO::FETCH_ASSOC);
		if($fetchall)
			return $req->fetchAll();
		else {
			return $req;
		}
	}
	/**
	 * Prépare et execute une requete d'insertion ou de mise à jour
	 * retourne vrai si la requete a été correctement effectuée
	 */
	public function exec(string $statement, array $attribus = null):bool{
		if(!$this->isNotSelect($statement)) throw new Exception("this function only execute statements, does not restitute values");
		if($this->transaction && $this->isAlter($statement)) throw new Exception("cannot edit table/databases in transaction, please commit/rollback before and relaunch transaction");
		if($attribus !== null)
			return $this->getPDO()->prepare($statement)->execute($attribus);
		else return $this->getPDO()->prepare($statement)->execute();
	}

	private function isNotSelect(string $statement){
		return strpos($statement, 'DELETE') !== false ||
			   strpos($statement, 'UPDATE') !== false ||
			   strpos($statement, 'INSERT INTO') !== false && strpos($statement, 'RETURNING') === false ||
			   strpos($statement, 'CREATE') !== false ||
			   strpos($statement, 'DROP') !== false ||
			   strpos($statement, 'ALTER') !== false;
	}
	private function isAlter(string $statement){
		return strpos($statement, 'CREATE') !== false ||
			   strpos($statement, 'DROP') !== false ||
			   strpos($statement, 'ALTER') !== false;
	}
}
 ?>
