<?php
namespace Database;
/**
 * Classe gérant le singleton de Database, il initie également la base de données et ses données si neécessaire
 */
class DatabaseHandler{

    private static $db; // Singleton

	public static function getDB(): Database{
		if(self::$db === null){
			require_once "DatabaseConfig.php";
			self::$db = new Database(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_NAME);
        }
        if(!self::isInitied(self::$db)) self::initTables(self::$db);
		return self::$db;
    }
    
    private static function isInitied(Database $db):bool{ // check if database tables exists
         return ($db->query("SELECT COUNT(*) as cpt FROM pg_tables WHERE schemaname='public'",null,null,false)->fetch(\PDO::FETCH_ASSOC)['cpt'] >= 8) &&
        ($db->query("SELECT COUNT(*) as cpt FROM information_schema.views
where table_schema not in ('information_schema', 'pg_catalog')",null,null,false)->fetch(\PDO::FETCH_ASSOC)['cpt']>=1);
    }

    private static function initTables(Database $db){ //call initializing file
        $db->executeFile(self::getFile("initializeDatabase.pgsql"));
        $files = glob(self::getFile("functions/*.pgsql"));
        foreach($files as $file){
            $db->executeFile($file);
        }
    }

    private static function getFile(string $name){
        return __DIR__."/../../sql/".$name;
    }

}
?>