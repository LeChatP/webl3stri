<!DOCTYPE html>
<html class="has-navbar-fixed-top">
<?php
echo $renderer->render("header");
?>

<body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="section">
        <div class="container">
            <div class="content">
                <h1 class="title">Comment Parier ?</h1>
                <hr>
                <p>Rien de plus simple ! Connectez-vous ou inscrivez-vous sur le site. A l'inscription vous avez 50 points. Les points peuvent être négatifs. Puis allez dans l'onglet Parier</p>
                <h2 class="title">Faire un Pari</h2>
                <p>Une fois dans le menu des paris, il faut sélectionner un match à venir.
                    Ceci vous amènera devant un formulaire requierant les essais, les transformations, et les pénalités de chaque équipe vous souhaitez parier.
                </p>
                <h2 class="title">Quel sont les règles?</h2>
                <p>Une fois que le match est terminé et que les scores ont été entrés.
                    </br><b>Si le pari a prédit le bon gagnant</b> vous obtiendrez : 
                    <ul>
                        <li>Un pari où tous les champs sont parfaitement prédit et vous fais gagner <span class="has-text-success has-text-weight-bold">300 points</span>.</li>
                        <li>Un pari où le score est égal au score effectué vous fais gagner <span class="has-text-success has-text-weight-bold">100 points</span>.</li>
                        <li>Un pari où le score est correct pour une seule equipe est prédit vous fais gagner <span class="has-text-success has-text-weight-bold">25 points</span>.</li>
                        <li>Un pari où le score est incorrect fais gagner <span class="has-text-success has-text-weight-bold">10 points</span></li>
                    </ul>
                    Si vous n'avez pas prédit le bon gagnant vous perdez <span class="has-text-danger has-text-weight-bold">25 points</span>. <br/>
                    Dans le cas d'un match nul il ne sera donc pas possible d'obtenir 25 points ni 10 points, soit vous obtenez les 300, 100 points respectant les conditions ci-dessus soit vous perdez 25 pour un score incorrect
                </p>
                <h2 class="title">Que gagne-ton?</h2>
                <p>
                    Vos points sont affichés à coté de votre pseudo dans les commentaires et vous aurez la gloire d'avoir votre pseudo en haut du classement. Vous allez pouvoir crâner devant les amis et qu'il vous donne le préfixe du maître des paris de rugby. Vous pouvez également ajouter votre score sur votre CV et montrer qui c'est le patron en entretien.
                    Vous avez la possibilité aussi de vous la péter en soirée en énumérant toutes le prédictions correctes et passer la meilleure soirée de tous les temps. Je peux même vous conseiller d'ajouter ce score et le préfixe sur des sites de rencontre, nos experts affirment que les probabilités de rencontrer sa moitié est augmentée de 27%.
                </p>
            </div>
        </div>
    </section>
    <?php
    echo $renderer->render('footer');
    ?>
</body>

</html>