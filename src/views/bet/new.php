<!DOCTYPE html>
<html class="has-navbar-fixed-top">
    <head>
    <?php
echo $renderer->render("header");
?>
    </head>
    <body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="section">
        <div class="container">
            <h1 class="title">Parier sur le match <?php echo $equipeA;?> - <?php echo $equipeB;?></h1>

            <p class="content">Je vous rapelle que les paris en ligne sont des paris fictifs, en aucun cas nous ne nous
                demanderons de l'argent.<br/>
            </p>

        </div>
    </section>
    <section class="section">
        <div class="container">
            <h1 class="title">Formulaire de paris</h1>
<?php if(isset($error)) {
?>
<script type="text/javascript">
document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    $notification = $delete.parentNode;

    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});
</script>
<div class="notification is-danger">
  <button class="delete"></button>
  <? echo $error; ?>
</div>
<?php }
?>
            <form action="<?php echo $router->urlPOST("bet.post",["id"=>$id]) ?>" method="post">
                <div class="columns">
                    <div class="column is-half has-text-centered is-large"><?php echo $equipeA;?></th>
                    </div>
                    <div class="column is-half has-text-centered is-large"><?php echo $equipeB;?></th>
                    </div>
                </div>
                <div class="columns">
                    <div class="column is-half">
                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Essais</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input class="input <?php if(isset($essaisAerror)) echo $essaisAerror; ?>" type="number" name="essaisA" <?php if(isset($essaisA)) echo "value='".$essaisA."'"; ?> />
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-football-ball"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Transformations</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input class="input <?php if(isset($transAerror)) echo $transAerror; ?>" type="number" name="transA" <?php if(isset($transA)) echo "value='".$transA."'"; ?> />
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-exchange-alt"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Penalités</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input class="input <?php if(isset($penaAerror)) echo $penaAerror; ?>" type="number" name="penaA" <?php if(isset($penaA)) echo "value='".$penaA."'"; ?> />
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-exclamation"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="control">
                            <button class="button is-link is-light is-fullwidth">Reset</button>
                          </div>
                    </div>
                    <div class="column is-half">
                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Essais</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input class="input <?php if(isset($essaisBerror)) echo $essaisBerror; ?>" type="number" name="essaisB" <?php if(isset($essaisB)) echo "value='".$essaisB."'"; ?> />
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-football-ball"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Transformations</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control has-icons-left">
                                        <input class="input <?php if(isset($transBerror)) echo $transBerror; ?>" type="number" name="transB" <?php if(isset($transB)) echo "value='".$transB."'"; ?> />
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-exchange-alt"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="field is-horizontal">
                            <div class="field-label">
                                <label class="label">Penalités</label>
                            </div>
                            <div class="field-body">
                                <div class="field">
                                    <p class="control  has-icons-left">
                                        <input class="input <?php if(isset($penaBerror)) echo $penaBerror; ?>" type="number" name="penaB" <?php if(isset($penaB)) echo "value='".$penaB."'"; ?> />
                                        <span class="icon is-small is-left">
                                            <i class="fas fa-exclamation"></i>
                                        </span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="control">
                            <button class="button is-link is-fullwidth">Parier</button>
                          </div>
                    </div>
                </div>
                <div class="field is-grouped">
                
                  
                </div>
        </div>
        </form>
    </section>
        <?php
        echo $renderer->render("footer")
        ?>
    </body>
</html>