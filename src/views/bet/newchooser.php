<!DOCTYPE html>
<html class="has-navbar-fixed-top">
    <head>
    <?php
echo $renderer->render("header");
?>
    </head>
    <body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="section">
        <div class="container">
            <h1 class="title">Parier un match</h1>
            <p class="content">Choissisez un match sur lequel parier</p>
        </div>
    </section>
    <section class="section">
        <div class="container">
            <h1 class="title">Matchs à venir</h1>
            <form method='GET'>
            <?php
            $tab = new Tableau();
            $tab->setColumnHeader(["Année","EquipeA","EquipeB","Date Prévue","Selectionner"]);
            $tab->addTableCss("is-fullwidth");
            foreach($matchincoming as $match){
                $tab->setLine([$match->__get("annee"),$match->__get("equipea"),$match->__get("equipeb"),$match->__get("debut"),'<div class="buttons is-right"><button type="submit" class="button is-link is-light is-right" formaction="'.$router->urlGET("bet.match",["id"=>$match->__get("idmatch")]).'">Parier</div>']);
            }
            echo $tab->getTableau();
            ?>
            </form>
        </div>
    </section>
        <?php
        echo $renderer->render("footer")
        ?>
    </body>
</html>