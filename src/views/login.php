<!DOCTYPE html>
<html>

<head>
    <?php
    echo $renderer->render("header");
    ?>
</head>

<body>

    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container ">
                <div class="columns is-centered">
                    <div class="column is-5-tablet is-4-desktop is-3-widescreen">

                        <h3 class="title has-text-black has-text-centered">Connexion</h3>

                        <form class="box" method="POST">
                            <div class="field">
                                <label for="" class="label">Username</label>
                                <div class="control has-icons-left">
                                    <input type="text" placeholder="username" name="username" class="input <?php echo !$isknow ? "is-danger" : ""; ?>">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-user"></i>
                                    </span>
                                </div>
                            </div>

                            <div class="field">
                                <label for="" class="label">Password</label>
                                <div class="control has-icons-left">
                                    <input type="password" placeholder="*********" name="password" class="input <?php echo !$isknow ? "is-danger" : ""; ?>">
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <?php if (!$isknow) { ?>
                                    <p class="help is-danger">Nom d'utilisateur ou mot de passe incorrect !</p>
                                <?php } ?>
                            </div>

                            <div class="level options">
                                <a class="btn btn-link level-right" href="<?php echo $router->urlGET('register') ?>">Pas encore inscrit?</a>
                            </div>
                            <div class="field is-grouped">
                                <div class="control">
                                    <button class="button is-link">Login</button>
                                </div>
                                <div class="control">
                                    <button class="button is-link is-light" name="annuler" value="annuler">Annuler</button>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    echo $renderer->render('footer');
    ?>
</body>

</html>