<!DOCTYPE html>
<html class="has-navbar-fixed-top">
<?php
echo $renderer->render("header");
?>

<body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="section">
        <div class="container">
            <h1 class="title">Mon Compte</h1>
            <div class="content">
                <h2 class="title">Mes Informations</h2>
                <?php 
                $tab = new Tableau();
                $tab->setLine(["Nom d'utilisateur : ",$user->getNom()]);
                $tab->setLine(["Mot de passe : ","*********"]);
                $tab->setLine(["Administrateur : ",$access->is_admin()?"oui":"non"]);
                $tab->setLine(['<div class="buttons is-right"><button type="submit" class="button is-link is-light is-fullwidth" formaction="'.$router->urlGET("edituser").'">Modifier les informations</div>'])
                echo $tab->getTableau();
                ?>
            </div>
            <div class="content">
                
                <div class="tile is-ancestor">
                    <div class="tile is-parent">
                    <h2 class="title">Mes paris en cours</h1>
                <?php
                if(isset($currentparis)){
                    foreach ($currentparis as $pari){
                        $tab = new Tableau();

                        $tab->setColumnHeader(["Pays","Score","Essais","Transformations","Penalités"]);
                        $tab->setLineHeader();
                        $tab->setLine([$pari->get('equipea'),$pari->__get('scorea'),$pari->__get('essaia'),$pari->__get('transformationa'),$pari->__get('penalitea'),]);
                        $tab->setLine([$pari->get('equipeb'),$pari->__get('scoreb'),$pari->__get('essaib'),$pari->__get('transformationb'),$pari->__get('penaliteb'),]);
                        echo '<div class="tile is-child box">'.$tab->getTableau().'</div>';
                    }
                }
                else{
                    echo '<div class="tile is-child box"><p class="title"><i class="fas fa-sad-tear"></i> Aucun pari...<a href="'.$router->urlGET('bet.new').'">Faites vos jeux ici !</a></p></div>';
                }
                ?>
                    </div>
                    <div class="tile is-parent">
                    <h2 class="title">Mes paris terminés</h1>

                <?php
                if(isset($paris)){
                    foreach($paris as $pari){
                        $res = $results[$pari->__get('idmatch')]; //resultat
                        $gain = $points[$pari->__get('idmatch')];
                        $tab = new Tableau();

                        $tab->setColumnHeader(["Score","Essais","Transformations","Penalités"]);
                        $tab->setLineHeader();
                        $tab->setLine(["Parié"]);
                        $tab->setLine([$pari->__get('equipea'),$pari->__get('scorea'),$pari->__get('essaia'),$pari->__get('transformationa'),$pari->__get('penalitea')]);
                        $tab->setLine([$pari->__get('equipeb'),$pari->__get('scoreb'),$pari->__get('essaib'),$pari->__get('transformationb'),$pari->__get('penaliteb')]);
                        $tab->setLine(["Résultats"]);
                        $tab->setLine([$res->__get('equipea'),$res->__get('scorea'),$res->__get('essaia'),$res->__get('transformationa'),$res->__get('penalitea')]);
                        $tab->setLine([$res->__get('equipeb'),$res->__get('scoreb'),$res->__get('essaib'),$res->__get('transformationb'),$res->__get('penaliteb')]);
                        $tab->setLine(['<span class="title '.($gain > 0 ? "has-text-success" : "has-text-danger") .'"'.$gain.'</span>']);
                        echo '<div class="tile is-child box">'.$tab->getTableau().'</div>';
                    }
                }
                else{
                    echo '<div class="tile is-child box"><p class="title"><i class="fas fa-sad-tear"></i> Aucun pari...<a href="'.$router->urlGET('bet.new').'">Faites vos jeux ici !</a></p></div>';
                }
                ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php
    echo $renderer->render('footer');
    ?>
</body>

</html>