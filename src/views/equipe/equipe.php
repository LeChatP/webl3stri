<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>England</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
    <script defer src=" https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>

<section class="hero is-dark">
    <div class="hero-body" style="margin-left: 10%">
        <div class="tile">
            <img src="./England_rugby.png" style="height: 100px">
            <div class="tile is-vertical" style="margin-left: 10px">
                <div class="title" style="font-size: xxx-large">
                    England
                </div>
                <div class="subtitle">
                    World Ranking : <b>3rd</b>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="tile is-ancestor" style="background-color: whitesmoke">

    <div class="tile is-vertical box" style="margin: 5% 2.5% 5% 5%;box-shadow: 0 0 8px; background-color: white">

        <div style="text-align: center">
            <div class="title">Squad</div>
            <hr>
            <div>Head Coach : <b>Eddie Jones</b><br></div>
            <img src="./Eddie_Jones.jpg" style="height: 100px">
        </div>

        <br>

        <table class="table is-striped is-hoverable" style="margin-left: auto; margin-right: auto">
            <thead>
            <tr>
                <th>Player</th>
                <th>Postion</th>
                <th>Date of birth (age)</th>
                <th>Caps</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><b>Luke Cowan-Dickie</b></td>
                <td>Hooker</td>
                <td>20 June 1993 (aged 26)</td>
                <td>21</td>
            </tr>
            <tr>
                <td><b>Tom Dunn</b></td>
                <td>Hooker</td>
                <td>12 November 1992 (aged 27)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Jamie George</b></td>
                <td>Hooker</td>
                <td>20 October 1990 (aged 29)</td>
                <td>45</td>
            </tr>
            <tr>
                <td><b>Beno Obano</b></td>
                <td>Prop</td>
                <td>25 October 1994 (aged 25)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Ellis Genge</b></td>
                <td>Prop</td>
                <td>16 February 1995 (aged 24)</td>
                <td>14</td>
            </tr>
            <tr>
                <td><b>Joe Marler</b></td>
                <td>Prop</td>
                <td>7 July 1990 (aged 29)</td>
                <td>68</td>
            </tr>
            <tr>
                <td><b>Kyle Sinckler</b></td>
                <td>Prop</td>
                <td>30 March 1993 (aged 26)</td>
                <td>31</td>
            </tr>
            <tr>
                <td><b>Will Stuart</b></td>
                <td>Prop</td>
                <td>12 July 1996 (aged 23)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Mako Vunipola</b></td>
                <td>Prop</td>
                <td>14 January 1991 (aged 29)</td>
                <td>58</td>
            </tr>
            <tr>
                <td><b>Harry Williams</b></td>
                <td>Prop</td>
                <td>1 October 1991 (aged 28)</td>
                <td>18</td>
            </tr>
            <tr>
                <td><b>Charlie Ewels</b></td>
                <td>Lock</td>
                <td>29 June 1995 (aged 24)</td>
                <td>12</td>
            </tr>
            <tr>
                <td><b>George Kruis</b></td>
                <td>Lock</td>
                <td>22 February 1990 (aged 29)</td>
                <td>41</td>
            </tr>
            <tr>
                <td><b>Joe Launchbury</b></td>
                <td>Lock</td>
                <td>12 April 1991 (aged 28)</td>
                <td>62</td>
            </tr>
            <tr>
                <td><b>Courtney Lawes</b></td>
                <td>Lock</td>
                <td>23 February 1989 (aged 30)</td>
                <td>81</td>
            </tr>
            <tr>
                <td><b>Maro Itoje</b></td>
                <td>Lock</td>
                <td>28 October 1994 (aged 25)</td>
                <td>34</td>
            </tr>
            <tr>
                <td><b>Alexander Moon</b></td>
                <td>Lock</td>
                <td>6 September 1996 (aged 23)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Ted Hill</b></td>
                <td>Back row</td>
                <td>26 March 1999 (aged 20)</td>
                <td>1</td>
            </tr>
            <tr>
                <td><b>Tom Curry</b></td>
                <td>Back row</td>
                <td>15 June 1998 (aged 21)</td>
                <td>19</td>
            </tr>
            <tr>
                <td><b>Ben Earl</b></td>
                <td>Back row</td>
                <td>7 January 1998 (aged 22)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Lewis Ludlam</b></td>
                <td>Back row</td>
                <td>8 December 1995 (aged 24)</td>
                <td>6</td>
            </tr>
            <tr>
                <td><b>Sam Underhill</b></td>
                <td>Back row</td>
                <td>22 July 1996 (aged 23)</td>
                <td>15</td>
            </tr>
            <tr>
                <td><b>Willi Heinz</b></td>
                <td>Scrum-half</td>
                <td>24 November 1986 (aged 33)</td>
                <td>9</td>
            </tr>
            <tr>
                <td><b>Ben Youngs</b></td>
                <td>Scrum-half</td>
                <td>5 September 1989 (aged 30)</td>
                <td>95</td>
            </tr>
            <tr>
                <td><b>Owen Farrell</b></td>
                <td>Fly-half</td>
                <td>24 September 1991 (aged 28)</td>
                <td>79</td>
            </tr>
            <tr>
                <td><b>George Ford</b></td>
                <td>Fly-half</td>
                <td>16 March 1993 (aged 26)</td>
                <td>65</td>
            </tr>
            <tr>
                <td><b>Jacob Umaga</b></td>
                <td>Fly-half</td>
                <td>8 July 1998 (aged 21)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Ollie Devoto</b></td>
                <td>Centre</td>
                <td>22 September 1993 (aged 26)</td>
                <td>1</td>
            </tr>
            <tr>
                <td><b>Fraser Dingwall</b></td>
                <td>Centre</td>
                <td>7 April 1999 (aged 20)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Jonathan Joseph</b></td>
                <td>Centre</td>
                <td>21 May 1991 (aged 28)</td>
                <td>47</td>
            </tr>
            <tr>
                <td><b>Manu Tuilagi</b></td>
                <td>Centre</td>
                <td>18 May 1991 (aged 28)</td>
                <td>40</td>
            </tr>
            <tr>
                <td><b>Ollie Thorley</b></td>
                <td>Wing</td>
                <td>11 September 1996 (aged 23)</td>
                <td>0</td>
            </tr>
            <tr>
                <td><b>Jonny May</b></td>
                <td>Wing</td>
                <td>1 April 1990 (aged 29)</td>
                <td>52</td>
            </tr>
            <tr>
                <td><b>Anthony Watson</b></td>
                <td>Wing</td>
                <td>26 February 1994 (aged 25)</td>
                <td>42</td>
            </tr>
            <tr>
                <td><b>Elliot Daly</b></td>
                <td>Fullback</td>
                <td>8 October 1992 (aged 27)</td>
                <td>39</td>
            </tr>
            <tr>
                <td><b>George Furbank</b></td>
                <td>Fullback</td>
                <td>17 October 1996 (aged 23)</td>
                <td>0</td>
            </tr>
            </tbody>
        </table>

    </div>

    <div class="tile is-vertical" style="margin: 5% 5% 5% 2.5%">

        <div class="tile is-vertical box" style="box-shadow: 0 0 8px; background-color: white">

            <div style="text-align: center">
                <div class="title">Fixtures</div>
                <hr>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./France_rugby.png" style="height: 50px"><b>France - England </b><img src="./England_rugby.png" style="height: 50px"></div>
                <div>2 Feb 2020</div>
                <div style="font-size: small">Stade de France, Paris<br>Ref: Nigel Owens</div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./Scotland_rugby.png" style="height: 50px"><b> Scotland - England </b><img src="./England_rugby.png" style="height: 50px"></div>
                <div>8 Feb 2020</div>
                <div style="font-size: small">BT Murrayfield Stadium, Edinburgh<br>Ref: Pascal Gauzere</div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"><b> England - Ireland </b><img src="./Ireland_rugby.png" style="height: 50px"></div>
                <div>23 Feb 2020</div>
                <div style="font-size: small">Twickenham Stadium, London<br>Ref: Jaco Peyper</div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"><b> England - Wales </b><img src="./Wales_rugby.png" style="height: 50px"></div>
                <div>7 Mar 2020</div>
                <div style="font-size: small">Twickenham Stadium, London<br>Ref: Ben O'Keeffe</div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./Italy_rugby.png" style="height: 50px"><b> Italy - England </b><img src="./England_rugby.png" style="height: 50px"></div>
                <div>14 Mar 2020</div>
                <div style="font-size: small">Stadio Olimpico, Rome<br>Ref: N/A</div>
            </div>

        </div>

        <div class="tile is-vertical box" style="box-shadow: 0 0 8px; background-color: white">

            <div style="text-align: center">
                <div class="title">Latest Results</div>
                <hr>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"> England 12 - <b>32 South Africa </b><img src="./SA_rugby.png" style="height: 50px"></div>
                <div>2 Nov 2019</div>
                <div style="font-size: small">International Stadium Yokohama, Yokohama<br></div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"><b> England 19</b> - 7 New Zealand <img src="./NZ_rugby.png" style="height: 50px"></div>
                <div>26 Oct 2019</div>
                <div style="font-size: small">International Stadium Yokohama, Yokohama<br></div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"><b> England 40</b> - 16 Australia <img src="./Australia_rugby.png" style="height: 50px"></div>
                <div>19 Oct 2019</div>
                <div style="font-size: small">Ōita Stadium, Ōita<br></div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"><b> England 0 - 0 France </b><img src="./France_rugby.png" style="height: 50px"></div>
                <div>12 Oct 2019</div>
                <div style="font-size: small">International Stadium Yokohama, Yokohama<br></div>
            </div>

            <div class="tile is-vertical" style="text-align: center">
                <div style="font-size: x-large"><img src="./England_rugby.png" style="height: 50px"><b> England 39</b> - 10 Argentina <img src="./Argentina_rugby.png" style="height: 50px"></div>
                <div>5 Oct 2019</div>
                <div style="font-size: small">Tokyo Stadium, Chōfu<br></div>
            </div>

        </div>

    </div>


</div>

</html>
