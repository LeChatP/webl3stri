<html class="">
<?php
echo $renderer->render("header");
?>

<body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="hero is-danger is-fullheight">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Accès refusé
      </h1>
      <h2 class="subtitle">
        Vous ne devriez pas être ici, Big Brother is Watching you!
      </h2>
    </div>
  </div>


    <?php
    echo $renderer->render('footer');
    ?>
    </section>
</body>

</html>