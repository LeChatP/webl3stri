<html class="has-navbar-fixed-top">
<head>
<?php
echo $renderer->render("header");
?>
<script>
  document.addEventListener('DOMContentLoaded', () => {
  (document.querySelectorAll('.notification .delete') || []).forEach(($delete) => {
    $notification = $delete.parentNode;

    $delete.addEventListener('click', () => {
      $notification.parentNode.removeChild($notification);
    });
  });
});
</script>
</head>

<body>
    <?php
    echo $renderer->render('navbar');
    if(isset($info)){
        echo '<div class="notification '.$class.'">
        <button class="delete" ></button>
        '.$info.'
      </div>';
    }
    ?>
    <section class="section">
        <h1 class="title">Panel Administration</h1>
    <div class="box">
        <h2 class="title">Matchs en cours</h2>
        <form method='POST'>
    <?php
        $tableau = new Tableau();
        $tableau->setTableCss(["is-hoverable","is-fullwidth","is-striped","is-narrow"]);
        foreach($playing_matchs as $match){
            $tableau->setLine([$match->__get("equipea"),$match->__get("equipeb"),'<div class="buttons is-right"><button type="submit" class="button is-link is-light is-right" name="id" value="'.$match->__get("idmatch").'" formaction="'.$router->urlPOST("admin.match.result").'">Entrer les Scores</div>']);
        }
        echo $tableau->getTableau();
    ?>
        </form>
    </div>
    <div class="box">
        <h2 class="title">Matchs à prévoir</h2>
        <form method='POST'>
    <?php
        $tableau = new Tableau();
        $tableau->setTableCss(["is-hoverable","is-fullwidth","is-striped","is-narrow"]);
        foreach($incoming_matchs as $match){
            $tableau->setLine([$match->__get("equipea"),$match->__get("equipeb"),'<div class="buttons is-right"><button type="submit" class="button is-link is-light is-right" name="id" value="'.$match->__get("idmatch").'" formaction="'.$router->urlPOST("admin.match.run").'">Lancer le match</div>']);
        }
        echo $tableau->getTableau();
    ?>
        </form>
    </div>
    </section>

    <?php
    echo $renderer->render('footer');
    ?>
</body>

</html>