<!DOCTYPE html>
<html>

<head>
    <?php
    echo $renderer->render("header");
    ?>
    <script type="text/javascript" src=<?php echo $assets->getAsset("preview_image.js"); ?>></script>
</head>

<body>
    <section class="hero is-fullheight">
        <div class="hero-body">
            <div class="container ">
                <div class="columns is-centered">
                    <div class="column is-5-tablet is-4-desktop is-3-widescreen">

                    <h3 class="title has-text-black has-text-centered">Inscription</h3>

                        <form class="box" method="POST" enctype="multipart/form-data">
                            <div class="field">
                                <label class=" label">Nom d'utilisateur :</label>
                                <div class="control has-icons-left has-icons-right">
                                    <input type="text" placeholder="nom d'utilisateur" name="username" class="input <?php echo $isknow || $errInput ? "is-danger" : ""; ?>">
                                    <span class="icon is-small is-left">
                                        <i class="fas fa-user"></i>
                                    </span>
                                    <span class="icon is-small is-right">
                                        <i class="fas fa-check"></i>
                                    </span>
                                </div>
                                <?php if ($isknow) { ?>
                                    <p class="help is-danger">Ce nom d'utilisateur est déjà utilisé !</p>
                                <?php }
                                elseif ($errInput) { ?>
                                    <p class="help is-danger"> Nom d'utilisateur incorrecte !</p>
                                <?php } ?>
                            </div>

                            <div class="field">
                                <label class="label">Mot de passe :</label>
                                <div class="control has-icons-left has-icons-right">
                                    <input type="password" placeholder="*********" name="password" class="input <?php echo $errInput && !$isknow ? "is-danger" : ""; ?>">
                                    <span class="icon is-small is-left">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                    <span class="icon is-small is-right">
                                        <i class="fa fa-lock"></i>
                                    </span>
                                </div>
                                <?php if ($errInput && !$isknow) { ?>
                                    <p class="help is-danger">Votre mot de passe ne peut pas être vide ou contenir d'espace.</p>
                                <?php } ?>
                            </div>

                            <div class="field file <?php echo isset($uploadError) ? "is-danger" : ""; ?> has-name is-left has-name">
                                <label class="file-label">
                                    <input class="file-input" type="file" onchange="preview(this)" name="avatar"/>
                                    <span class="file-cta">
                                        <span class="file-icon">
                                            <i class="fas fa-upload"></i>
                                        </span>
                                        <span class="file-label">
                                            Photo (Optionnel)</span>
                                    </span>
                                </label>
                                <span class="file-name">
                                <figure class="image is-48x48">
                                    <?php echo isset($uploadError) ? $uploadError : ""; ?>
                                    <img id="thumbnail" src="" width="48" height="48" style="display:none" alt="preview image profil">
                                </figure>
                                </span>
                            </div>
                            <div class="level options">
                                <a class="btn btn-link level-right" href="<?php echo $router->urlGET('login') ?>">Déjà inscrit?</a>
                            </div>

                            <div class="field is-grouped">
                                <div class="control">
                                    <button class="button is-link">S'inscrire</button>
                                </div>
                                <div class="control">
                                    <button class="button is-link is-light" name="annuler" value="annuler">Annuler</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </section>

</body>

</html>