<html class="has-navbar-fixed-top">
<?php
echo $renderer->render("header");
?>

<body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="section">
    <div class="tile is-ancestor">
  <div class="tile is-vertical is-8">
    <div class="tile">
      <div class="tile is-parent is-vertical">
        <article class="tile is-child notification is-primary">
          <p class="title">CVC gèle son projet d'investissement de 340 millions d'euros pour le Tournoi</p>
          <p class="subtitle"></p>
          <figure class="image is-4by3">
          <img src="<?php echo $assets->getAsset("cvc.jpeg") ?>" alt="Tournoi des six nations" width="400" height="200"/></a>
          </figure>
        </article>
        <article class="tile is-child notification is-warning">
          <p class="title">Rugby et coronavirus : le Stade Toulousain demande de l'aide aux supporters en créant un “mur de soutien”</p>
          <p class="subtitle"></p>
        </article>
      </div>
      <div class="tile is-parent">
        <article class="tile is-child notification is-info">
          <p class="title">Et si on rejouait tout le Tournoi ?</p>
          <p class="subtitle">TOURNOI DES 6 NATIONS 2020 - CORONAVIRUS - C'est un scénario qui prend de l'ampleur côté hémisphère nord, 
            celui de rejouer entièrement le Tournoi des 6 Nations 2020 au mois de novembre.</p>
          <figure class="image is-4by3">
          <img src="<?php echo $assets->getAsset("covid.jpeg") ?>" alt="Tournoi des six nations" width="640" height="480"/></a>
          </figure>
        </article>
      </div>
    </div>
    <div class="tile is-parent">
      <article class="tile is-child notification is-danger">
        <p class="title">Solidarité : 55 981€ récoltés par l'UBB </p>
        <p class="subtitle">CORONAVIRUS - Le club bordelo-béglais a clôturé sa cagnotte solidaire aujourd'hui. Ce sont donc 55 981 € qui seront mis à disposition du CHU Bordeaux et de son personnel soignant. L'UBB est le deuxième club à dépasser les 50 000 € de dons après le Stade Toulousain (83 000 €) et montre une fois de plus la générosité des supporters de rugby.</p>
        <div class="content">
          <!-- Content -->
        </div>
      </article>
    </div>
  </div>
  <div class="tile is-parent">
    <article class="tile is-child notification is-success">
      <div class="content">
        <p class="title">Tournoi des 6 Nations féminin : le match Écosse - France reporté pour une suspicion de coronavirus</p>
        <p class="subtitle">Le match du Tournoi des 6 Nations féminin entre l'Écosse et la France prévu ce samedi soir à Glasgow a été reporté. Une joueuse écossaise a été testée positif au coronavirus.</p>
        <figure class="image is-4by3">
          <img src="<?php echo $assets->getAsset("feminin.jpeg") ?>" alt="Tournoi des six nations" /></a>
          </figure>
        <div class="content">
          <!-- Content -->
        </div>
      </div>
    </article>
  </div>
</div>
    </section>

    <?php
    echo $renderer->render('footer');
    ?>
</body>

</html>