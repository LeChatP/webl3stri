<!DOCTYPE html>
<html class="has-navbar-fixed-top">
  <head>
  <?php
echo $renderer->render("header");
?>
   </head>
   <body class="has-navbar-fixed-top">

   <?php
    echo $renderer->render('navbar');
    ?>
    
    <section class="section">
        <div class="container">
<h1 class="title">1/ L’HISTOIRE DU TOURNOI</h1>    

<p class="content">Le tournoi des six nations tire ses racines <strong>dès les premières rencontres internationales </strong>qui avaient lieu entre l’Angleterre et l’Écosse dans les années 1870.
    En 1884, c’est la première fois que l’Angleterre, l’Écosse, l’Irlande et le Pays de Galles se rencontrent lors d’une même saison.</p>

<p class="content">La France fait son entrée dans cette confrontation internationale en 1893 en disputant un match contre l'Angleterre. En 1910, la France est invitée officiellement à intégrer la compétition. 
    <strong>Le tournoi des 5 nations</strong> prend alors forme. Cette année là, ce sera l’Angleterre qui remporte le titre. </p>

<p class="content">En 1931, la France est exclue du tournoi par les nations anglophones pour des raisons de violence et de professionnalisme déguisé. 
        Elle le réintègre en 1939. La guerre stoppe néanmoins le tournoi jusqu’en 1947.<strong> En 1959</strong>, c’est une année historique pour la France qui remporte son premier tournoi. 
        Elle réalise par la même occasion <strong>le grand chelem</strong> , ce qui signifie qu’elle a remporté toutes ses rencontres durant le tournoi.
</p>
<p class="content">Après ça, c’est une domination écrasante du Pays de Galles qui remportera à <strong>dix reprises le tournoi entre 1964 et 1979</strong>. Les années 1980 sont elles dominées par la France qui gagne quatre tournois consécutifs.
     C’est à la suite de cette période que les Anglais surnomeront les confrontations France / Angleterre par le mot <strong>“crunch”</strong> qui signifie moment crucial.
     A ce moment là, les deux équipes sont considérées comme les plus fortes de l'hémisphère nord. Les rencontres entre anglais et français sont généralement très violentes et rugueuses. </p>
<p class="content">En 1993, pour la première fois, les points marqués durant toutes les rencontres sont pris en compte en cas d’égalité. <strong>Un trophée des cinq nations est décerné au vainqueur du tournoi. </strong></p>
<p class="content">En 1996, le tournoi devient la <strong>Coupe d'Europe des nations du rugby à XV </strong>mais garde son nom de tournoi des cinq nations. L'Angleterre devient la première nation championne d’Europe. </p>
<p class="content">En<strong> 2000</strong>, l’Italie rentre dans le tournoi qui change automatiquement de nom pour s’appeler <strong>tournoi des six nations</strong>. En 2003, l’Angleterre est la première à réaliser le grand chelem en remportant ses cinq matchs.</p>
<p class="content">Aujourd’hui la question d’une <strong>“montée / descente”</strong> est évoquée due notamment aux bonnes performances de l’équipe de Géorgie et d’une manière plus globale l’essor des nations mineures européennes 
    (Espagne, Roumanie, Belgique, Portugal, Russie). Les résultats des prochaines coupes du monde seront scrutés avec beaucoup d’attention.</p>

</div>
</section>
<section class="section">
    <div class="container">
<h1 class="title">2/ LES JOUEURS PHARES</h1>
<figure class="image is-230x230 is-pulled-right">
    <img src="<?php echo $assets->getAsset("WILKINSON.jpg"); ?>">
  </figure>
    <p class="content">Le tournoi des 6 nations est marqué par <strong>quelques joueurs phares</strong> ayant brillé lors de la compétition. </p>
<p class="content"><strong>Jonny Wilkinson</strong> (anglais), demi d’ouverture. Il est considéré comme le meilleur joueur à son poste dans les années 2003 / 2010, notamment pour sa régularité dans l'exercice des pénalités </p>
<p class="content"><strong>Brian O’Driscoll</strong> (Irlandais), centre. Il détient le record du plus grand nombre d’essais marqué dans la compétition et du plus grand nombre de matchs disputés. L’âme de l’équipe d’Irlande, un monument.</p>
<p class="content"><strong>Sergio Parisse</strong> (Italien), troisième ligne. Très grand numéro 8 qui détient le record de matchs disputés dans un tournoi. Il devrait le battre prochainement étant donné qu’il est encore en activité. </p>
<p class="content"><strong>Serge Blanco </strong>(Français), arrière. Le rugbyman le plus célèbre du XV de France. Il est le joueur français à avoir remporté le plus de tournoi. </p>
<p class="content"><strong>Gareth Edwards</strong> (Gallois), demi de mêlée. </p>
<p class="content"><strong>Gavin Hastings</strong> (Ecosse), arrière. </p>
<p class="content"> </p>

</div>
</section>

<section class="section">
    <div class="container">
        <h1 class="title">3/ LES STADES DU TOURNOI</h1>
<div class="field-item even" property="content:encoded"><p class="content">Comme 6 nations, le tournoi se déroule dans 6 stades emblématiques du rugby. </p>
    <p class="content"><strong>Aviva Stadium</strong> à Dublin : IRLANDE</p>
    <p class="content"><strong>Murrayfield Stadium</strong> à Édimbourg: ÉCOSSE</p>
    <p class="content"><strong>Millennium Stadium</strong> à Cardiff: PAYS DE GALLES</p>
    <p class="content"><strong>Stade olympique</strong> à Rome: ITALIE</p>
    <p class="content"><strong>Stade de France </strong>à Saint-Denis: FRANCE</p>
    <p class="content"><strong>Twickenham</strong> à Londres: ANGLETERRE</p>
</div>
</section>
<?php
    echo $renderer->render('footer');
    ?>
</body>
</html>