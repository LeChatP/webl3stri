<?php
use Router\AccessLevel;
use Exceptions\FileNotFoundException;
?>
<nav class="navbar is-fixed-top is-primary">
  <div class="navbar-brand">
  <a class="navbar-item" href="<?php echo $router->urlGET('home');?>">
    <img src="<?php echo $assets->getAsset("favicon.png"); ?>" alt="Tournoi des six nations" width="40" /></a>
    <div class="navbar-burger burger" data-target="navbar">
        <span></span>
        <span></span>
        <span></span>
      </div>

  </div>

  <div class="navbar-menu" id="navbar">
    <div class="navbar-start">
      <a class="navbar-item" href="<?php echo $router->urlGET('history');?>">
        <span class="icon is-medium">
          <i class="fas fa-book"></i>
        </span> Son Histoire
      </a>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          <span class="icon is-medium">
            <i class="fas fa-euro-sign"> </i>
          </span> Parions
        </a>
        <div class="navbar-dropdown is-boxed">
          <a class="navbar-item" href="<?php echo $router->urlGET('bet.start');?>">
            Comment parier ?
          </a>
          <a class="navbar-item" href="<?php if($router->url_is_accessed_by('GET','bet.new',$access)) echo $router->urlGET('bet.new'); else echo $router->urlGET('login');?>">
            Faites vos jeux!
          </a>

        </div>
      </div>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          <span class="icon is-medium">
            <i class="fas fa-football-ball"></i>
          </span> Les équipes
        </a>
        <div class="navbar-dropdown is-boxed">
          <?php
          foreach ($equipes as $eq){
            echo '<a class="navbar-item" href="'.$router->urlGET('team',["name"=>$eq->__get("pays")]).'">'.$eq->__get("pays").'</a>';
          }
          ?>
        </div>
      </div>

      <a class="navbar-item" href="<?php echo $router->urlGET('history');?>">
        <span class="icon is-medium">
          <i class="fas fa-users"></i>
        </span>
        Les joueurs du Tournoi
      </a>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          <span class="icon is-medium">
            <i class="far fa-chart-bar"></i>
          </span> Statistiques du Tournoi
        </a>
        <div class="navbar-dropdown is-boxed">
          <a class="navbar-item" href="<?php echo $router->urlGET('ranking.teams'); ?>">
            Classement par équipes
          </a>
        </div>
      </div>
      <div class="navbar-item has-dropdown is-hoverable">
        <a class="navbar-link">
          <span class="icon is-medium">
            <i class="fas fa-chart-area"></i>
          </span> Statistiques des paris
        </a>
        <div class="navbar-dropdown is-boxed">
          <a class="navbar-item" href="<?php echo $router->urlGET('ranking.bet'); ?>">
            Classement Parieurs
          </a>

        </div>
      </div>

    </div>

    <div class="navbar-end ">
      <!--Fin navbar-->
      <?php
      if($access->has_access(new AccessLevel(AccessLevel::ADMIN))){
      ?>
      <div class="navbar-item">
        <div class="field">
          <p class="control">
            <a class="button is-info" href="<?php echo $router->urlGET('admin.panel'); ?>">
              <span class="icon is-medium">
                <i class="fas fa-user-cog"></i>
              </span>
              <span>Admin</span>
            </a>
          </p>
        </div>
      </div>
      <?php
      }
      if($access->has_access(new AccessLevel(AccessLevel::MEMBER))){
        ?>
          <p class="navbar-item">
            <span>Bienvenue <?php echo $user->getNom(); ?></span>
          </p>
          <?php
          $icon  = '<p class="navbar-item">
          <span class="icon is-medium">
            <i class="fas fa-user"></i>
          </span>
        </p>';
          if ($user->getPhoto() != null){
            try{
              $photo = $assets->getAsset($user->getPhoto());
              echo '<figure class="navbar-item image">
              <img class="is-rounded" src="'.$photo.'" width="24" height="24" />
            </figure>';
            }catch(FileNotFoundException $exp){
              echo $icon;
            }
          }else{
            echo $icon;
          }
          ?>
          <div class="navbar-item">
            <a class="button is-primary" href="<?php echo $router->urlGET('logout'); ?>">
              <span class="icon is-medium">
                <i class="fas fa-sign-out-alt"></i>
              </span>
              <span>Déconnexion</span>
            </a>
          </div>
          </p>
        </div>
      </div>
        <?php
      }else{
      ?>
      <div class="navbar-item">
        <div class="field is-grouped">
          <p class="control">
            <a class="button is-primary" href="<?php echo $router->urlGET('login'); ?>">
              <span class="icon is-medium">
                <i class="fas fa-sign-in-alt"></i>
              </span>
              <span>Connexion</span>
            </a>
          </p>
          <p class="control">
            <a class="button is-info" href="<?php echo $router->urlGET('register'); ?>">
              <span class="icon is-medium">
                <i class="fas fa-plus-circle"></i>
              </span>
              <span>Inscription</span>
            </a>
          </p>
        </div>
      </div>
      <?php
      }
      ?>
    </div>
  </div>
</nav>