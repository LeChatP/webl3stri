<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo $assets->getAsset('stylesheet.css');?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo $assets->getAsset('favicon.png');?>">
    <link rel="icon" type="image/png" href="<?php echo $assets->getAsset('favicon.png');?>" sizes="32x32">
    
    <link rel="icon" type="image/png" href="<?php echo $assets->getAsset('favicon.png');?>" sizes="16x16"><link rel="manifest" href="https://bulma.io/favicons/manifest.json?v=201701041855">
    <link rel="mask-icon" href="<?php echo $assets->getAsset('favicon.png');?>" color="#00d1b2">
    <link rel="shortcut icon" href="<?php echo $assets->getAsset('favicon.ico');?>">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <script>
        document.addEventListener('DOMContentLoaded', () => {

            // Get all "navbar-burger" elements
            const $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);
        
            // Check if there are any navbar burgers
            if ($navbarBurgers.length > 0) {
        
            // Add a click event on each of them
            $navbarBurgers.forEach( el => {
                el.addEventListener('click', () => {
        
                // Get the target from the "data-target" attribute
                const target = el.dataset.target;
                const $target = document.getElementById(target);
        
                // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
                el.classList.toggle('is-active');
                $target.classList.toggle('is-active');
        
                });
            });
            }
        
        });
    </script>
</head>