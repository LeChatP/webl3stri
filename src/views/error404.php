<html class="">
<?php
echo $renderer->render("header");
?>

<body>
    <?php
    echo $renderer->render('navbar');
    ?>
    <section class="hero is-warning is-fullheight">
  <div class="hero-body">
    <div class="container">
      <h1 class="title">
        Page introuvable
      </h1>
      <h2 class="subtitle">
        Vous ne trouverez définitivement rien sur cette page
      </h2>
    </div>
  </div>


    <?php
    echo $renderer->render('footer');
    ?>
    </section>
</body>

</html>