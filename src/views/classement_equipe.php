<!DOCTYPE html>
<html class="has-navbar-fixed-top">

<?php
echo $renderer->render("header");
?>

  <title>Classement des meilleures équipes</title>
 

<body>
<?php
    echo $renderer->render('navbar');
    ?>
<div class="hero is-fullheight">
  <h1 class="title">Classement des équipes du Tournoi</h1>
  <div class="table-container">
    <?php

    $Tab_classement = new Tableau();
    $Tab_classement->setTableCss(["is-narrow","is-fullwidth","is-striped","is-hoverable"]);
    $Tab_classement->setColumnHeader(["Pays","Points","Gagnés","Nuls","Perdus"]);
    $Tab_classement->setLineHeader();
    foreach ($classement as $value){
      $Tab_classement->setLine ([$value["pays"],$value["points"],$value["wins"],$value["nuls"],$value["perdus"]]); 
    }
  echo $Tab_classement->getTableau();

    //TODO créer un objet Tableau
    //TODO effectuer une boucle for a partir de la variable classement
    //Indice : regarde les commentaires dans database/Database.php ligne 80, ça peut t'aider
    //Indice : echo var_dump($classement); pour connaître le contenu de ta variable, ce qui peut t'aider également
    ?>
  </div>
  <?php
    echo $renderer->render('footer');
    ?>
</div>
</body>

</html>