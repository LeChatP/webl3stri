<?php
namespace Models;
use Contents\Pari;
/**
 * Requetes SQL pour les Paris des utilisateurs
 */
class BetService extends Service {
    public function __construct()
    {
        parent::__construct();
    }

    public function newPari($idMatch,$idU,$numEqA,$essaisA,$transA,$penaA,$numEqB,$essaisB,$transB,$penaB):bool{
        $date = date('Y-m-d H:i:s');
        return $this->db->exec("INSERT INTO Parier(idMatch,idU,idScore,datepari) VALUES (:match,:user,(INSERT INTO Score(numEq,essai,transformation,penalite) VALUES (:eqA,:essaiA,:transA,:penaA) RETURNING idScore),:datepari),
                                                                                        (:match,:user,(INSERT INTO Score(numEq,essai,transformation,penalite) VALUES (:eqB,:essaiB,:transB,:penaB) RETURNING idScore),:datepari)",
                        ["match"=>$idMatch,
                         "user"=>$idU,
                         "datepari"=>$date,
                         "eqA"=>$numEqA,
                         "eqB"=>$numEqB,
                         "essaiA"=>$essaisA,
                         "essaiB"=>$essaisB,
                         "transA"=>$transA,
                         "transB"=>$transB,
                         "penaA"=>$penaA,
                         "penaB"=>$penaB]); // on ajoute 2 ligne par pari. Les contraintes SQL empêchent l'ajout de plus de 2 ligne pour un utilisateur sur un pari
    }

    public function getParisUser($idU) : array{
        return $this->db->query("SELECT * FROM Paris WHERE idU = :idU",["idU"=>$idU],"Pari");
    }

    public function getPariMatchUser($idU,$idMatch): ?Pari{
        $req = $this->db->query("SELECT * FROM Paris WHERE idU = :idU AND idMatch = :idMatch",["idU"=>$idU,"idMatch"=>$idMatch],"Pari");
        if(count($req) > 0)
            return $req[0];
        else return null;
    }



}
?>