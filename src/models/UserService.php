<?php
namespace Models;
use Router\AccessLevel;
use Contents\Utilisateur;
use Database\DatabaseHandler;
/**
 * Classe regroupant les requetes SQL en rapport à la gestion et utilisation des données utilisateur.
 */
class UserService extends Service{

    private $id;

    public function __construct(int $id){
        parent::__construct();
        $this->id = $id;
    }

    /**
     * cette fonction va ajouter un utilisateur dans la base de données
     * et retourner son identifiant
     */
    public function createUser($username,$mdp,$img): int{
        //on insère un utilisateur en retournant son identifiant, le résultat est un fetchAll donc un tableau de tableau aasociatifs est renvoyé
        $this->id=intval($this->db->query("INSERT INTO Utilisateur(nomu,mdp,admin,pdp) VALUES (:username,:mdp,false,:img) RETURNING idu",
                                        ["username"=>$username,"mdp"=>$mdp,"img"=>$img]
                                        )[0]['idu']);
        return $this->id;
    }

    /**
     * Cette fonction supprime un utilisateur
     */
    public function deleteUser(int $idUser){
        return $this->db->exec("DELETE FROM Utilisateur WHERE idu = :id",["id"=>$idUser]);
    }

    /**
     * Retourne le niveau d'accès de l'utilisateur
     */
    public function get_level():int{
        // si l'identifiant n'est pas égal à zéro (sinon visiteur) et que l'identifiant de l'utilisateur est admin dans la base de données alors le niveau d'accès est Admin, autrement
        if($this->id != 0){
            $u = $this->db->query("SELECT admin FROM Utilisateur WHERE idu = :idU",["idU"=>$this->id]);
            if(!empty($u)){
                return boolval($u[0]['admin']) ? AccessLevel::ADMIN : AccessLevel::MEMBER;
            }
        }
        return AccessLevel::VISITOR;
    }

    public function getUtilisateur(){
        if($this->id == 0) {
            return null;
        }else{
            $u = $this->db->query("SELECT * FROM Utilisateur WHERE idu = :idu",["idu"=>$this->id]);
            if(!empty($u)){
                $user = $u[0];
                return new Utilisateur($user['idu'],$user['nomu'],$user['mdp'], (boolval($user['admin']) ? AccessLevel::ADMIN : AccessLevel::MEMBER),$user['inscription'],$user['pdp'] == 'null' ? null : $user['pdp']);
            }
            return null;
        }
    }

    public static function getId($nomu){
        $id = DatabaseHandler::getDB()->query("SELECT idu FROM Utilisateur WHERE nomu = :nomu", ["nomu"=>$nomu]);
        if(!empty($id))
            return $id[0]['idu'];
        return null;
    }
}
?>