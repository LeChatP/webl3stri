<?php

namespace Models;
use Contents\Equipe;
/**
 * Classe regroupant les requetes SQL pour l'obtention des equipes du tournoi
 */
class TeamService extends Service {
    public function __construct()
    {
        parent::__construct();
    }

    public function getAllPays(): array{ 
        return $this->db->query("SELECT * FROM Equipe",[],"Equipe");
    }

    public function getEquipe(string $equipe): Equipe{
        return $this->db->query("SELECT * FROM Equipe WHERE pays = :pays",["pays"=>$equipe],"Equipe")[0];
    }
    /**
     * Ajoute un joueur à la base
     */
    public function ajouterJoueur($nom,$prenom,$photo=null,$naissance=null,$taille=null,$poids=null,$numEq=null):int{
        $args = ["nom"=>$nom,"prenom"=>$prenom];
        if($photo!=null)$args["photo"] = $photo;
        if($naissance != null)$args["taille"]= $taille;
        if($poids !=null)$args["poids"]=$poids;
        if($numEq != null)$args["numEq"]=$numEq;
        $req = "INSERT INTO Joueur(".explode(",",array_keys($args))."VALUES (";
        foreach(array_keys($args) as $key){
            $req.=$key." = :".$key." AND ";
        }
        $req = substr($req, 0, -4).") RETURNING numLicence";
        return $this->db->query($req,$args)[0]["numlicence"];
    }
    /**
     * Modifie un joueur de la base
     */
    public function modifierJoueur($numLicence,$nom=null,$prenom=null,$photo=null,$naissance=null,$taille=null,$poids=null,$numEq=null):int{
        $args = [];
        if($nom!=null)$args["nom"]= $nom;
        if($prenom!=null)$args["prenom"]= $prenom;
        if($photo!=null)$args["photo"] = $photo;
        if($naissance != null)$args["taille"]= $taille;
        if($poids !=null)$args["poids"]=$poids;
        if($numEq != null)$args["numEq"]=$numEq;
        $req = "UPDATE Joueur SET ";
        foreach(array_keys($args) as $key){
            $req.=$key." = :".$key." ,";
        }
        $req = substr($req, 0, -1)." WHERE numLicence = :numLicence";
        $args["numLicence"]=$numLicence;
        return $this->db->exec($req,$args);
    }

    /**
     * retire le joueur de son equipe
     */
    public function retraiteJoueur($numLicence):bool{
        return $this->db->exec("UPDATE Joueur SET numEq = NULL WHERE numLicence = :num",["num"=>$numLicence]);
    }
    /**
     * supprime un joueur de la base
     */
    public function supprimerJoueur($numLicence):bool{
        return $this->db->exec("DELETE FROM Joueur WHERE numLicence = :num",["num"=>$numLicence]);
    }
}
?>