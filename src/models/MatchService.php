<?php
namespace Models;
use Contents\Match;

/**
 * Requetes SQL pour la gestion/lecture des Matchs
 */
class MatchService extends Service {
    public function __construct()
    {
        parent::__construct();
    }

    public function addMatch($annee,$equipeA,$equipeB,$debut):int{
        return $this->db->query("INSERT INTO Match(annee,debut,idEtat,idscore_equipea,idscore_equipeb) VALUES (:annee,:debut,1,(INSERT INTO Score(numEq) VALUES (:eqA) RETURNING idScore),(INSERT INTO Score(numEq) VALUES (:eqB) RETURNING idScore) RETURNING idMatch",
                                ["annee"=>$annee,"debut"=>$debut,"eqA"=>$equipeA,"eqB"=>$equipeB])[0]["idmatch"];
    }

    public function setPlayed($idMatch,$eqA,$essaiA,$transA,$penaA,$eqB,$essaiB,$transB,$penaB,$fin):bool{
        try{
            assert($this->db->beginTransaction());
            assert($this->db->exec("UPDATE Score SET essai = :essai, transformation = :trans, penalite = :pena WHERE idMatch = :match AND numEq = :numEq",
            ["essai"=>$essaiA,"trans"=>$transA,"pena"=>$penaA,"match"=>$idMatch,"numEq"=>$eqA]));
            assert($this->db->exec("UPDATE Score SET essai = :essai, transformation = :trans, penalite = :pena WHERE idMatch = :match AND numEq = :numEq",
            ["essai"=>$essaiB,"trans"=>$transB,"pena"=>$penaB,"match"=>$idMatch,"numEq"=>$eqB]));
            assert($this->db->exec("UPDATE Match SET idEtat = 2, fin = :fin WHERE idMatch=:match",["fin"=>$fin,"match"=>$idMatch]));
            assert($this->db->commit());
        }catch(\AssertionError $e){
            $this->db->rollback();
            return false;
        }
        return true;
    }

    public function setPlaying($idMatch){
        return $this->db->exec("UPDATE Match SET idEtat = 3 WHERE idMatch = :match",["match"=>$idMatch]);
    }

    public function report($idMatch){
        return $this->db->exec("UPDATE Match SET idEtat = 4 WHERE idMatch = :match",["match"=>$idMatch]);
    }

    public function editScore($idMatch,$idScore,$eqA=null,$essaiA=null,$transA=null,$penaA=null){
        $args=[];
        if($eqA != NULL) $args["numEq"]=$eqA;
        if($essaiA != NULL) $args["essai"]=$essaiA;
        if($transA != NULL) $args["transformation"]=$transA;
        if($penaA != NULL) $args["penalite"]=$penaA;
        $req = "UPDATE Score SET ";
        foreach(array_keys($args) as $key){
            $req.=$key." = :".$key." ,";
        }
        $req = substr($req, 0, -1)." WHERE idMatch = :match AND idScore = :idScore";
        $args["match"] = $idMatch;
        $args["idScore"] = $idScore;
        return $this->db->exec($req,$args);
    }

    public function editMatch($idMatch,$idScoreA=null,$eqA=null,$essaiA=null,$transA=null,$penaA=null,$idScoreB=null,$eqB=null,$essaiB=null,$transB=null,$penaB=null,$etat=null,$fin=null){
        try{
            assert($this->db->beginTransaction());
            if($idScoreA != NULL)
                assert($this->editScore($idMatch,$idScoreA,$eqA,$essaiA,$transA,$penaA));
            if($idScoreB != NULL)
                assert($this->editScore($idMatch,$idScoreB,$eqB,$essaiB,$transB,$penaB));
            assert($this->db->exec("UPDATE Match SET idEtat = :etat, fin = :fin WHERE idMatch=:match",["etat"=>$etat,"fin"=>$fin,"match"=>$idMatch]));
            assert($this->db->commit());
        }catch(\AssertionError $e){
            $this->db->rollback();
            return false;
        }
        return true;
    }

    public function deleteMatch($idMatch){
        try{
            assert($this->db->beginTransaction());
            assert($this->db->exec("DELETE FROM Commenter WHERE idMatch = :match",["match"=>$idMatch]));
            assert($this->db->exec("DELETE FROM Parier WHERE idMatch = :match",["match"=>$idMatch]));
            assert($this->db->exec("DELETE FROM Match WHERE idMatch = :match",["match"=>$idMatch]));
            assert($this->db->commit());
        }catch(\AssertionError $e){
            $this->db->rollback();
            return false;
        }
        return true;
    }

    public function getMatch($idMatch):Match{
        return $this->db->query("SELECT m.* , sa.numEq as numEqA,ea.pays as equipeA, ea.photo as Photoa, sa.essai as essaiA, sa.transformation as transformationA, sa.penalite as penaliteA,
                                        sb.numEq as numEqB,eb.pays as equipeB, eb.photo as Photob, sb.essai as essaiB, sb.transformation as transformationB, sb.penalite as penaliteB
                                        FROM eMatch m
                                        INNER JOIN Score sa ON m.idscore_equipea = sa.idScore
                                        INNER JOIN Score sb ON m.idscore_equipeb = sb.idScore
                                        INNER JOIN Equipe ea ON sa.numEq = ea.numEq
                                        INNER JOIN Equipe eb ON sb.numEq = eb.numEq
                                        WHERE m.idMatch = :match",["match"=>$idMatch],"Match")[0];
    }

    public function getAllMatch():array{
        return $this->db->query("SELECT m.* , sa.numEq as numEqA,ea.pays as equipeA, ea.photo as Photoa, sa.essai as essaiA, sa.transformation as transformationA, sa.penalite as penaliteA,
                                        sb.numEq as numEqB,eb.pays as equipeB, eb.photo as Photob, sb.essai as essaiB, sb.transformation as transformationB, sb.penalite as penaliteB
                                        FROM eMatch m
                                        INNER JOIN Score sa ON m.idscore_equipea = sa.idScore
                                        INNER JOIN Score sb ON m.idscore_equipeb = sb.idScore
                                        INNER JOIN Equipe ea ON sa.numEq = ea.numEq
                                        INNER JOIN Equipe eb ON sb.numEq = eb.numEq
                                        ",null,"Match");
    }

    public function getUnplayedMatchs():array{
        return $this->db->query("SELECT m.* , sa.numEq as numEqA,ea.pays as equipeA, ea.photo as Photoa, sa.essai as essaiA, sa.transformation as transformationA, sa.penalite as penaliteA,
                                        sb.numEq as numEqB,eb.pays as equipeB, eb.photo as Photob, sb.essai as essaiB, sb.transformation as transformationB, sb.penalite as penaliteB
                                        FROM eMatch m
                                        INNER JOIN Score sa ON m.idscore_equipea = sa.idScore
                                        INNER JOIN Score sb ON m.idscore_equipeb = sb.idScore
                                        INNER JOIN Equipe ea ON sa.numEq = ea.numEq
                                        INNER JOIN Equipe eb ON sb.numEq = eb.numEq
                                        WHERE m.idEtat=1 OR m.idEtat=4",null,"Match");
    }
    public function getPlayedMatchs():array{
        return $this->db->query("SELECT m.* , sa.numEq as numEqA,ea.pays as equipeA, ea.photo as Photoa, sa.essai as essaiA, sa.transformation as transformationA, sa.penalite as penaliteA,
                                        sb.numEq as numEqB,eb.pays as equipeB, eb.photo as Photob, sb.essai as essaiB, sb.transformation as transformationB, sb.penalite as penaliteB
                                        FROM eMatch m
                                        INNER JOIN Score sa ON m.idscore_equipea = sa.idScore
                                        INNER JOIN Score sb ON m.idscore_equipeb = sb.idScore
                                        INNER JOIN Equipe ea ON sa.numEq = ea.numEq
                                        INNER JOIN Equipe eb ON sb.numEq = eb.numEq
                                        WHERE m.idEtat=2",null,"Match");
    }
    public function getPlayingMatchs():array{
        return $this->db->query("SELECT m.* , sa.numEq as numEqA,ea.pays as equipeA, ea.photo as Photoa, sa.essai as essaiA, sa.transformation as transformationA, sa.penalite as penaliteA,
                                        sb.numEq as numEqB,eb.pays as equipeB, eb.photo as Photob, sb.essai as essaiB, sb.transformation as transformationB, sb.penalite as penaliteB
                                        FROM eMatch m
                                        INNER JOIN Score sa ON m.idscore_equipea = sa.idScore
                                        INNER JOIN Score sb ON m.idscore_equipeb = sb.idScore
                                        INNER JOIN Equipe ea ON sa.numEq = ea.numEq
                                        INNER JOIN Equipe eb ON sb.numEq = eb.numEq
                                        WHERE m.idEtat=3",null,"Match");
    }



}
?>