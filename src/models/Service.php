<?php
namespace Models;

/**
 * Classe généralisant tous les Classes de Models. Il conserve la base de données appelée par tous les models
 */

abstract class Service {
    protected $db;
    protected function __construct(){
        $this->db = \Database\DatabaseHandler::getDB();
    }
}
?>