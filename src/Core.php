<?php
use Router\Renderer;
use Router\Router;
use Router\Assets;
use Router\AccessLevel;
use Models\UserService;
use Models\TeamService;
use Modules\FactoryModule;
/**
 * Classe Principale du site internet, instancie tous les modules et retrouve la session utilisateur si existante
 */
class Core{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var Assets
     */
    private $assets;
    /**
     * @var AccessLevel
     */
    private $access;
    /**
     * @var Renderer
     */
    private $renderer;
    private $modules = [];

    /**
     * Constructeur, appelé a chaque nouvelle page
     */
    public function __construct(){
        Security::checkSessionCreated();
        $this->router = new Router($_SERVER['REQUEST_URI'],[$this,"notFound"],[$this,"unauthorized"]);
        $this->assets = new Assets();
        $id =0;
        if(isset($_SESSION['idUser']))
            $id = $_SESSION['idUser'];
        $userService = new UserService($id);
        $this->access = new AccessLevel($userService->get_level(),$id);
        $user = $userService->getUtilisateur();
        $teamService = new TeamService();
        $this->renderer = new Renderer("views/",$this->router,$this->assets,$this->access,["equipes"=>$teamService->getAllPays(),"user"=>$user]);
        //on dirige le root vers la Page Home
        $this->modules($this->access);
    }
    /**
     * Exécute la fonction du module en lui passant les variables en paramètre
     */
    public function show(){
        $callable = $this->getRouter()->getCallable($this->access);
        $params = $this->getRouter()->getParams($this->access);
        if (is_array($params))
            $response = call_user_func_array($callable,$params);
        else $response = call_user_func($callable);
        return $response;
    }
    /**
     * Charge tous les modules du site
     */
    private function modules($user){
        $factory = new FactoryModule($this->router,$this->renderer,$this->access);
        $factory->getModule("Main");
        $factory->getModule("Admin");
        $factory->getModule("Bet");
        $factory->getModule("Ranking");
        $factory->getModule("Team");
    }
    /**
     * Page 404 lorsque la page est introuvable
     * @return int 404
     */
    public function notFound(){
        echo $this->renderer->render("error404");
        return 404;
    }
    /**
     * Page 403 lorsque la page existe mais n'est pas accessible.
     * @return int 403
     */
    public function unauthorized(){
        echo $this->renderer->render("error403");
        return 403;
    }

    /**
     * Récupère le router
     */
    public function getRouter(){
        return $this->router;
    }

}
?>
