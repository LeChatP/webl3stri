<?php
namespace Contents;
/**
 * Correspond à une table Equipe dans la base de données
 * Lecture seule
 */
class Equipe{
    /**
     * @var int
     */
    private $numEq;
    /**
     * @var string
     */
    private $pays;
    /**
     * @var string
     */
    private $photo;
    public function __get($property){
        return $this->$property;
    }
}
?>