<?php

namespace Contents;
/**
 * Contient tous les attributs définissant un match
 * Lecture seule
 */
class Match {
    /**
     * identifiant du match
     * @var int
     */
    private $idmatch;
    /**
     * identifiant premier score
     * @var int
     */
    private $idscore_equipea;
    /**
     * Numéro de l'équipe
     * @var int
     */
    private $numeqa;
    /**
     * Nom du pays de l'équipea
     * @var string
     */
    private $equipea;
    /**
     * Photo de l'equipea
     * @var string
     */
    private $photoa;
    /**
     * essais par l'equipea
     * @var int
     */
    private $essaia;
    /**
     * transformations par l'equipea
     * @var int
     */
    private $transformationa;
    /**
     * penalités recus à l'equipea
     * @var int
     */
	private $penalitea;
    /**
     * identifiant deuxieme score
     * @var int
     */
    private $idscore_equipeb;
    /**
     * numero de l'equipeB
     * @var int
     */
    private $numeqb;
    /**
     * Nom du pays de l'équipea
     * @var string
     */
    private $equipeb;
    /**
     * Photo de l'equipea
     * @var string
     */
    private $photob;
    /**
     * essais de l'equipeb
     * @var int
     */
    private $essaib;
    /**
     * transformations de l'equipeB
     * @var int
     */
    private $transformationb;
    /**
     * penalités de l'equipeB
     * @var int
     */
	private $penaliteb;
    /**
     * identifiant etat du match
     * @var int
     */
    private $idetat;
    /**
     * identifiant du lieu
     * @var int
     */
    private $idlieu;
    /**
     * année
     * @var int
     */
    private $annee;
    /**
     * debut
     * @var string
     */
    private $debut;
    /**
     * fin
     * @var string
     */
    private $fin;

    public function __get($property){
        return $this->$property;
    }
}
?>