<?php
namespace Contents;
/**
 * Correspond à tous les élements nécéessaires à un pari
 * Lecture seule
 */
class Pari {
    /**
     * @var integer
     **/
    private $idu;	
    /**
     * @var integer
     **/
    private $idmatch;	
    /**
     * @var string
     **/
    private $datepari;	
    /**
     * @var integer
     **/
    private $equipea;	
    /**
     * @var integer
     **/
    private $scorea;	
    /**
     * @var integer
     **/
    private $essaia;	
    /**
     * @var integer
     **/
    private $transformationa;	
    /**
     * @var integer
     **/
    private $penalitea;	
    /**
     * @var integer
     **/
    private $equipeb;	
    /**
     * @var integer
     **/
    private $scoreb;	
    /**
     * @var integer
     **/
    private $essaib;	
    /**
     * @var integer
     **/
    private $transformationb;	
    /**
     * @var integer
     **/
    private $penaliteb;
    
    /**
     * @var integer
     */
    private $points;

    public function __get($property){
        return $this->$property;
    }

}

?>