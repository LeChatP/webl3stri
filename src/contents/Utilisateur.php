<?php
namespace Contents;
use Router\AccessLevel;
/**
 * Classe d'un utilisateur déjà inscrit
 * Lecture Seule
 */
class Utilisateur{
    /**
     * identifiant d'utilisateur
     * @var int
     */
    private $idUtilisateur;
    /**
     * Nom d'utilisateur
     * @var string
     */
    private $nomUtilisateur;
    /**
     * Mot de passe de l'utilisateur
     * @var string
     */
    private $password;
    /**
     * @var AccessLevel
     */
    private $accessLevel;
    /**
     * Date inscription
     * @var string
     */
    private $inscription;
    /**
     * Chemin vers photo de profil
     * @var mixed
     */
    private $photo;

    public function __construct(int $id,string $nom,string $password,int $accesslevel,string $inscription,$photo = null){
        $this->idUtilisateur = $id;
        $this->nomUtilisateur = $nom;
        $this->password = $password;
        $this->accessLevel = new AccessLevel($accesslevel,$id);
        $this->inscription = $inscription;
        $this->photo = $photo;
    }

    public function __get($property){
        return $this->$property;
    }

    public function getId():int{
        return $this->idUtilisateur;
    }

    public function getNom():string{
        return $this->nomUtilisateur;
    }

    public function getPassword(){
        return $this->password;
    }

    public function getAccessLevel():AccessLevel{
        return $this->accessLevel;
    }

    public function getInscription():string{
        return $this->inscription;
    }

    public function getPhoto(){
        return $this->photo;
    }
}
?>