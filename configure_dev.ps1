Add-Type -AssemblyName System.IO.Compression.FileSystem
function Unzip
{
    param([string]$zipfile, [string]$outpath)

    [System.IO.Compression.ZipFile]::ExtractToDirectory($zipfile, $outpath)
}

(New-Object System.Net.WebClient).DownloadFile("https://windows.php.net/downloads/releases/php-7.3.15-nts-Win32-VC15-x64.zip", "php.zip")
Unzip "php.zip" ".\php"
Remove-Item "php.zip"

Set-Content -Path ".vscode\settings.json" -Value '{"php.executablePath": "${workspaceRoot}/php/php.exe"}' -Force

.\php\php.exe -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
.\php\php.exe -r "if (hash_file('sha384', 'composer-setup.php') === 'e0012edf3e80b6978849f5eff0d4b4e4c79ff1609dd1e613307e16318854d24ae64f26d17af3ef0bf7cfb710ca74755a') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
.\php\php.exe composer-setup.php --install-dir=.\php
.\php\php.exe -r "unlink('composer-setup.php');"