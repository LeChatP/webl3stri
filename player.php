<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Player-info!</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.2/css/bulma.min.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
  </head>
  <body>

  <section class="hero">
    <div class="hero-body">
      <div class="container">
        <h1 class="title">
          Jonny Wilkinson
        </h1>
        <h2 class="subtitle">
          Description
        </h2>
      </div>
    </div>
  </section>


  <div class="card">
    <div class="card-content">
      <p class="title">

      </p>
      <p class="subtitle">
        Description du joueur
      </p>
    </div>
    <footer class="card-footer">
      <p class="card-footer-item">
        <span>
          <img src="/images/WILKINSON.jpg" alt="" />
        </span>
      </p>
      <p class="card-footer-item">
        <span>
          Nom : Jonathan Peter <br>
          Prenom : Wilkinson  <br>
          Naissance : 25 mai 1979  <br>
          Taille : 1,78 m  <br>
          Poids : 89 Kg

        </span>
      </p>
    </footer>
  </div>



  </body>
</html>
