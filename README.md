# Projet Web dynamique

[![pipeline status](https://gitlab.com/LeChatP/webl3stri/badges/master/pipeline.svg)](https://gitlab.com/LeChatP/webl3stri/-/commits/master)
 [![coverage report](https://gitlab.com/LeChatP/webl3stri/badges/master/coverage.svg)](https://gitlab.com/LeChatP/webl3stri/-/commits/master)

A compléter par ex. avec l'URL du site une fois celui-ci hébergé.

## Étapes nécessaires à l'utilisation du dépôt Git

### Spécification de votre identité Git

```bash
git config --global user.name "Prénom Nom"
git config --global user.email "votre adresse de courriel"
```

(chaque commande sans le dernier argument permet de vérifier chaque paramètre de votre identité)

### Spécification de votre éditeur "préféré"

Il s'agit de l'éditeur qui sera lancé pour l'édition des messages de commit (ex. `gedit`):

```bash
git config --global core.editor votre_éditeur
```

### Génération de clé SSH

```bash
ssh-keygen -t ed25519 -a 100 -f ~/.ssh/id_ed25519_gitlab_inetdoc_net -C "MY own key for GitLab"
```

### Copie de la clé publique

Il s'agit du contenu du fichier
`~/.ssh/id_ed25519_gitlab_inetdoc_net.pub` à copier dans
le champ *Key* de la page de votre
[profil utilisateur](https://gitlab.inetdoc.net/profile/keys)

### Spécification d'une configuration SSH

Il s'agit d'affecter le numéro de port à utiiser dans le fichier `~/.ssh/config` pour les accès Git :

```Conf
Host gitlab.inetdoc.net
	Port 2148
	User git
	IdentityFile ~/.ssh/id_ed25519_gitlab_inetdoc_net
	IdentitiesOnly yes
```

### Clonage du dépôt

```bash
git clone git@gitlab.inetdoc.net:projet-web-dynamique/g2-b/web.git
```

## Conseils d'utilisation

### Introduction à GitLab

[Vidéo d'introduction à GitLab](http://mbret.net/stri/l3/projet_web_php_bd/intro_gitlab.mp4)

## Environnement de développement

### Installation

#### **Linux est plus que recommandé pour développer en PHP**

Pour installer les dépendances de développement sous linux il suffit d'exécuter **configure_dev.sh** sous ubuntu, ceci vous éviteras d'installer tous les outils à la main. Si toutefois vous êtes sur linux ou sur Windows, vous pouvez toujours essayer d'installer ces dépendances. il n'est pas garanti que vous ayez tous les outils dans cette liste :

* PHP7.3 (sudo apt install php7.3 php7.3-dev)
  * pour Windows : [télécharger PHP](https://windows.php.net/downloads/releases/php-7.2.11-nts-Win32-VC15-x64.zip) et décompresser dans le projet

* [Docker](https://docs.docker.com/v17.12/install/#supported-platforms) Ubuntu 18.xx et supérieur : sudo apt install docker.io docker-compose
  * [Docker-compose](https://docs.docker.com/compose/install/#install-compose)
* [Visual Studio Code](https://code.visualstudio.com/Download) + PHP Debug + PHP Extension Pack + PHP Intellisense extensions
  * Pour Windows si VSCode indique que l'éxecutable PHP n'est pas trouvé :
    * Cliquer sur open settings cliquer sur Extensions > PHP > Executable Path > edit in settings.json
    * placer la souris sur "php.validate.executablePath" puis cliquer sur le crayon qui apparait et cliquer sur "Copy to settings"
    * Sur la ligne qui est apparue à droite : remplacer null par le chemin qui mène à l'executable php téléchargé précedemment "C:\\LeCheminVersPHP" (préciser les double-quotes et doubler les antislash)
  * Pour Linux : voir installer PHP7.3 et redémarrer VSCode

une fois tous ces programmes installés il suffit de d'éxecuter la commande : "docker-compose up" et suite à cette commande une longue série d'installation se fera et a la fin le serveur web sera opérationnel :

* localhost:8080 pour le serveur web
* localhost:8081 pour la gestion base de données

### Utilisation

Pour utiliser le debugger :

* Ctrl + Shift + D
* En haut a droite de la flèche verte cliquer sur add configuration et sélectionner Listen for XDebug
* placer des breakpoints dans le code a debugger
* Cliquer sur la flèche verte, puis ouvrir la page php à debugger, et magie dans l'IDE
* une petite barre avec différents icones en haut est apparue elle permettent de gérer le debug
* à gauche il y a les variables, le stack, et les breakpoints

## Intégration Continue

Ce projet est en intégration continue avec une couverture de code de tests, pour vérifier la couverture du code, il faut executer la commande :

docker exec -ti pw-php phpunit tests/ --coverage-text --colors=never --process-isolation --coverage-html /logs

Cette commande permettra de connaitre le pourcentage de couverture du code et de lancer les tests

## Génération de style

Pour générer le css :

cat charte.scss | node_modules/node-sass/bin/node-sass --output-style compressed > src/public/css/stylesheet.css

### **Attention la couverture du code se fait sur toutes les lignes de code donc si une ligne n'est pas executée par les tests dans une fonction, la fonction testée sera considérée comme non couverte**
