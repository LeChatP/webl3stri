if ! grep -rq vscode /etc/apt/sources.list /etc/apt/sources.list.d/*; then
    sudo apt install software-properties-common apt-transport-https wget
    wget -q https://packages.microsoft.com/keys/microsoft.asc -O- | sudo apt-key add -
    sudo add-apt-repository "deb [arch=amd64] https://packages.microsoft.com/repos/vscode stable main"
    sudo apt update && sudo apt install code 
fi
sudo apt update && sudo apt install git docker.io docker-compose php7.3 php7.3-dev php7.3-pgsql php7.3-curl composer phpunit
code --install-extension bmewburn.vscode-intelephense-client --install-extension CoenraadS.bracket-pair-colorizer-2 --install-extension esbenp.prettier-vscode --install-extension felixfbecker.php-debug --install-extension felixfbecker.php-intellisense --install-extension hbenl.vscode-test-explorer --install-extension recca0120.vscode-phpunit --install-extension rifi2k.format-html-in-php --force 
composer install