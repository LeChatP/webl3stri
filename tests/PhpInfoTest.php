<?php
use PHPUnit\Framework\TestCase;
/**
 * This class will test if php modules are activated
 */
class PhpinfoTest extends TestCase
{
    protected $phpi;
    protected $ini;

    protected function setUp():void{
        $this->phpi = get_loaded_extensions();
    }

    protected function tearDown():void{
        $this->phpi = null;
    }
    
    public function testPDOInstalled(){
        $this->assertTrue(in_array('PDO', $this->phpi));
    }

    public function testPostgresInstalled(){
        $this->assertTrue(in_array('pgsql', $this->phpi));
    }

    public function testXDebugInstalled(){
        $this->assertTrue(in_array('Zend OPcache', $this->phpi));
    }

    public function testcURLInstalled(){
        $this->assertTrue(in_array('curl', $this->phpi));
    }
}
?>