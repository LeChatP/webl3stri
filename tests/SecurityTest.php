<?php

use PHPUnit\Framework\TestCase;

class SecurityTest extends TestCase{
    public function testProtectPass(){
        $clear = 'securepass';
        $this->assertNotEquals($pass=Security::protectPass($clear),$pass2=Security::protectPass($clear));
        $this->assertEquals(1,Security::checkPassword($clear,$pass));
        $this->assertEquals(1,Security::checkPassword($clear,$pass2));
    }

    public function testSessionHijack(){
        $_SERVER['HTTP_USER_AGENT'] = 'ORIGINAL';
        Security::checkSession();
        $_SERVER['HTTP_USER_AGENT'] = 'OTHERONE';
        Security::checkSession();
        $this->assertSame(PHP_SESSION_NONE,session_status());
    }

    public function testRegenerateSessionExpired(){
        $_SERVER['HTTP_USER_AGENT'] = 'ORIGINAL';
        Security::checkSession();
        $id = session_id();
        $_SESSION['CREATED'] -= 2000;
        Security::checkSession();
        $this->assertNotEquals($id,session_id());
    }
}
?>