<?php

use PHPUnit\Framework\Exception;

/**
 * This class will test if paths exists
 */
class DocumentContentTest extends TestPageCase
{
    public function testIndexContent(){
        $contents = $this->callPage("GET","/");
        $this->assertNotEmpty($contents);
    }
}
?>