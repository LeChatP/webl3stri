<?php
use PHPUnit\Framework\TestCase;

use Router\AccessLevel;
use Router\Router;
/**
 * @covers Router\Router
 * @covers Router\AccessLevel
 * @covers Router\Route
 */
class RouterTest extends TestCase {
    /**
     * @var AccessLevel
     */
    private $access;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var bool
     */
    private $called;
    /**
     * @var bool
     */
    private $called404;
    /**
     * @var bool
     */
    private $called403;

    public function setUp(){
        $this->router = new Router("/",array($this,"page404"),array($this,"page403"));
        $this->access = new AccessLevel();
        $this->called = false;
        $this->called404 = false;
        $this->called403 = false;
    }

    public function tearDown(){
        unset($this->router);
        unset($this->called);
        unset($this->access);
    }

    public function testVisitorDefaultGET(){
        $this->router->get("/","home",[$this,"getThis"]);
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access));
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertTrue(call_user_func($this->router->getCallable($this->access)));
        $this->assertTrue($this->called);
        $this->assertFalse($this->called404);
        $this->assertFalse($this->called403);
    }

    public function testVisitorDefaultPOST(){
        $this->router->post("/","home",[$this,"getThis"]);
        $_SERVER['REQUEST_METHOD'] = "POST";
        $this->assertNotNull($this->router->getCallable($this->access));
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertTrue(call_user_func($this->router->getCallable($this->access)));
        $this->assertTrue($this->called);
        $this->assertFalse($this->called404);
        $this->assertFalse($this->called403);
    }

    /**
     * Teste qu'un visiteur ne peut accéder à aucun chemin de type POST à partir de GET
     * et retourne la page 404 (à ne pas confondre avec le code 404)
     */
    public function testVisitorGETAccessToPOSTPath(){
        $this->router->post("/","home",[$this,"getThis"]);
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access));
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertFalse(call_user_func($this->router->getCallable($this->access)));
        $this->assertFalse($this->called);
        $this->assertTrue($this->called404);
        $this->assertFalse($this->called403);
    }

    /**
     * Teste qu'un visiteur ne peut accéder à aucun chemin de type POST à partir de GET
     * et retourne la page 404 (à ne pas confondre avec le code 404)
     */
    public function testVisitorPOSTAccessToGETPath(){
        $this->router->get("/","home",[$this,"getThis"]);
        $_SERVER['REQUEST_METHOD'] = "POST";
        $this->assertNotNull($this->router->getCallable($this->access));
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertFalse(call_user_func($this->router->getCallable($this->access)));
        $this->assertFalse($this->called);
        $this->assertTrue($this->called404);
        $this->assertFalse($this->called403); // 403 doit etre appellé
    }

    /**
     * Teste que l'accès est refusé pour une page nécéssitant être membre retourne 403
     */
    public function testVisitorAccessDeniedMEMBER(){
        $this->router->get("/","home",[$this,"getThis"],[],new AccessLevel(AccessLevel::MEMBER));
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access)); // la fonction redirigée doit toujours être existante
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertFalse(call_user_func($this->router->getCallable($this->access))); // 403 doit etre appellé, cette fonction retourne faux
        $this->assertFalse($this->called); // la fonction getThis ne doit pas être appellée
        $this->assertFalse($this->called404);// pas de page 404
        $this->assertTrue($this->called403); // 403 doit etre appellé
    }

    /**
     * Teste que l'accès est refusé pour une page nécéssitant être membre retourne 403
     */
    public function testVisitorAccessDeniedOWNER(){
        $this->router->get("/","home",[$this,"getThis"],[],new AccessLevel(AccessLevel::OWNER));
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access)); // la fonction redirigée doit toujours être existante
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertFalse(call_user_func($this->router->getCallable($this->access))); // 403 doit etre appellé, cette fonction retourne faux
        $this->assertFalse($this->called); // la fonction getThis ne doit pas être appellée
        $this->assertFalse($this->called404);// pas de page 404
        $this->assertTrue($this->called403); // 403 doit etre appellé
    }
    /**
     * Teste que l'accès est refusé pour une page nécéssitant être membre retourne 403
     */
    public function testVisitorAccessDeniedADMIN(){
        $this->router->get("/","home",[$this,"getThis"],[],new AccessLevel(AccessLevel::ADMIN));
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access)); // la fonction redirigée doit toujours être existante
        $this->assertIsCallable($this->router->getCallable($this->access)); // la fonction doit être appellable
        $this->assertFalse(call_user_func($this->router->getCallable($this->access))); // 403 doit etre appellé, cette fonction retourne faux
        $this->assertFalse($this->called); // la fonction getThis ne doit pas être appellée
        $this->assertFalse($this->called404);// pas de page 404
        $this->assertTrue($this->called403); // 403 doit etre appellé
    }

    /**
     * Teste le droit d'accès aux routes
     */
    public function testOwnerAccessGranted(){
        $this->access = new AccessLevel(AccessLevel::MEMBER,333);
        $this->router->get("/","home",[$this,"getThis"],[],new AccessLevel(AccessLevel::OWNER,333));
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access)); // la fonction redirigée doit toujours être existante
        $this->assertTrue(call_user_func($this->router->getCallable($this->access))); // 403 doit etre appellé, cette fonction retourne faux
        $this->assertTrue($this->called); // la fonction getThis ne doit pas être appellée
        $this->assertFalse($this->called404);// pas de page 404
        $this->assertFalse($this->called403); // 403 doit etre appellé
    }

    /**
     * Teste le droit d'accès aux routes
     */
    public function testOwnerAdminAccessGranted(){
        $this->access = new AccessLevel(AccessLevel::ADMIN,333);
        $this->router->get("/","home",[$this,"getThis"],[],new AccessLevel(AccessLevel::OWNER,333));
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->assertNotNull($this->router->getCallable($this->access)); // la fonction redirigée doit toujours être existante
        $this->assertTrue(call_user_func($this->router->getCallable($this->access))); // 403 doit etre appellé, cette fonction retourne faux
        $this->assertTrue($this->called); // la fonction getThis ne doit pas être appellée
        $this->assertFalse($this->called404);// pas de page 404
        $this->assertFalse($this->called403); // 403 doit etre appelé
    }

    /**
     * Teste les paramètres d'un chemin
     */
    public function testGETParams(){
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->router = new Router("/test/10/test",array($this,"page404"),array($this,"page403"));
        $this->router->get("/test/:one/:two","home",[$this,"getThis"],["one"=>"[0-9]+"]);
        $params = $this->router->getParams($this->access);
        $this->assertIsArray($params);
        $this->assertEquals("10",$params[0]);
        $this->assertEquals("test",$params[1]);
    }

    /**
     * Teste les paramètres d'un chemin avec un paramètre incorrect
     */
    public function testIncorrectGETParams(){
        $_SERVER['REQUEST_METHOD'] = "GET";
        $this->router = new Router("/test/10asa",array($this,"page404"),array($this,"page403"));
        $this->router->get("/test/:one","home",[$this,"getThis"],["one"=>"[0-9]+"]);
        $params = $this->router->getParams($this->access);
        $this->assertNull($params);
    }

    /**
     * Teste la récupération d'url GET a partir du nom du chemin
     */
    public function testGETUrl(){
        $url = "/testValue";
        $this->router->get($url,"home",[$this,"getThis"]);
        $this->assertEquals($this->router->urlGET("home"),$url);
    }

    public function testGETUrlWithParams(){
        $url = "/test/:one/:two";
        $this->router->get($url,"home",[$this,"getThis"]);
        $this->assertEquals($this->router->urlGET("home",["one"=>"10","two"=>"test"]),"/test/10/test");
    }

    /**
     * Teste la récupération d'url POST a partir du nom du chemin
     */
    public function testPOSTUrl(){
        $url = "/testValue";
        $this->router->post($url,"home",[$this,"getThis"]);
        $this->assertEquals($this->router->urlPOST("home"),$url);
    }

    /**
     * Teste la récupération d'url POST a partir du nom du chemin
     * @expectedException PHPUnit\Framework\Error\Error
     */
    public function testErrorTriggered(){
        $this->router->post("/","home",[$this,"getThis"]);
        $this->router->urlPOST("another");
    }
    
    public function getThis(){
        $this->called = true;
        return true;
    }

    public function page404(){
        $this->called404 = true;
        return false;
    }

    public function page403(){
        $this->called403 = true;
        return false;
    }
}

?>