<?php

use PHPUnit\Framework\TestCase;

use Router\AccessLevel;
use Router\Router;
use Router\Renderer;
use Router\Assets;
/**
 * @covers Router\Renderer
 */
class RendererTest extends TestCase{
    /**
     * @var Renderer
     */
    private $renderer;

    /**
     * @var Router
     */
    private $router;
    /**
     * @var AccessLevel
     */
    private $access;
    /**
     * @var Assets
     */
    private $assets;
    /**
     * @var array
     */
    private $params;

    /**
     * @var int
     */
    public static $called;

    public function setUp(){
        $this->router = new Router("/",[$this,"null"],[$this,"null"]);
        $this->router->get("/testChemin","test",[$this,"null"]);
        $this->access = new AccessLevel();
        $this->assets = new Assets();
        $this->params = ["param1"=>true,"param2"=>"1"];
        self::$called = 0;
        $this->renderer = new Renderer("../tests/.views/",$this->router,$this->assets,$this->access,$this->params);
    }

    public function tearDown(){
        $this->renderer = null;
        self::$called = 0;
    }

    public function testRendererBasic(){
        $result = $this->renderer->render("renderHelloworldTest");
        $this->assertEquals(1,self::$called);
        $this->assertEquals("Hello World",$result);
    }

    public function testRendererContainsRouter(){
        $result = $this->renderer->render("renderRouterTest");
        $this->assertEquals(1,self::$called);
        $this->assertEquals("true",$result);
    }

    public function testRendererContainsAccess(){
        $result = $this->renderer->render("renderAccessTest");
        $this->assertEquals(1,self::$called);
        $this->assertEquals("true",$result);
    }

    public function testRendererContainsAssets(){
        $result = $this->renderer->render("renderAssetsTest");
        $this->assertEquals(1,self::$called);
        $this->assertEquals("true",$result);
    }

    public function testRendererContainsObjectParams(){
        $result = $this->renderer->render("renderParamObjectTest");
        $this->assertEquals(1,self::$called);
        $this->assertEquals("true",$result);
    }

    public function testRendererContainsCalledParams(){
        $this->renderer->addParam("called2","1");
        $result = $this->renderer->render("renderParamCalledTest",["called1"=>true]);
        $this->assertEquals(1,self::$called);
        $this->assertEquals("true",$result);
    }

    function null(){}

}
?>