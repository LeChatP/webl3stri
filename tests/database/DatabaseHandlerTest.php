<?php
use PHPUnit\Framework\TestCase;
use Database\DatabaseHandler;
/**
 * @covers Database\DatabaseHandler
 */
class DatabaseHandlerTest extends TestCase {
    public function testHandler(){
        $this->assertInstanceOf("Database\Database",DatabaseHandler::getDB());        
    }
    public function testInit(){
        $this->assertTrue(DatabaseHandler::getDB()->executeFile(__DIR__."/databaseTest.pgsql"));
        DatabaseHandler::getDB();
        $this->assertTrue(DatabaseHandler::getDB()->executeFile(__DIR__."/databaseTest.pgsql"));
    }
    public function testGetConfig(){
        include_once __DIR__.'/../../src/database/DatabaseConfig.php';
        $this->assertTrue(defined("DB_USERNAME"));
        $this->assertTrue(defined("DB_NAME"));
        $this->assertTrue(defined("DB_PASSWORD"));
        $this->assertTrue(defined("DB_SERVER"));
    }
}