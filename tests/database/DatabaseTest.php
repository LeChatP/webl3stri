<?php
use PHPUnit\Framework\TestCase;
use Database\Database;
/**
 * @covers Database\Database
 */
class DatabaseTest extends TestCase {

    protected $db;

    protected function setUp()
    {
        include_once __DIR__.'/../../src/database/DatabaseConfig.php';
        $this->db = new Database(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_NAME);
        //we want Database object that takes host,user,password,db in parameter
    }

    protected function tearDown()
    {
        $this->db->rollback(); // we want to rollback if any changes are done
        $this->db->close(); //we want to be able to close database
        $this->db = null;
    }

    /**
     * Test transaction functions
     */
    public function testPDOtransaction(){
        $this->assertTrue($this->db->beginTransaction()); // we want create transactions in test
    }

    public function testPDOrollback(){
        $this->assertTrue($this->db->beginTransaction());
        $this->assertTrue($this->db->rollback()); //we want to be able to test rollback
        $this->assertFalse($this->db->rollback()); //double rollback must return false because we can't rollback twice
    }

    public function testPDOcommit(){
        $this->assertTrue($this->db->beginTransaction());
        $this->assertTrue($this->db->commit()); //we want to be able to commit
        $this->assertFalse($this->db->commit()); //double commit is return false, same as rollback
    }

    /**
     * Test Close database
     */
    public function testClose(){
        $this->db->close(); // no error if no connection opened
        $this->assertTrue($this->db->beginTransaction()); // no error if open connection after close
        $this->db->close(); // no error if closing normal connection
    }
    /**
     * Test Execute statements with differents forms
     */
    public function testPDOExec(){
        $this->assertTrue($this->db->exec("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)"));
        $this->db->beginTransaction();
        $this->assertTrue($this->db->exec("INSERT INTO Test(col1) VALUES (?)",array("blabla")));
        $this->assertTrue($this->db->exec("INSERT INTO Test(col1) VALUES (:blabla)",["blabla"=>"anotherBlabla"]));
        $res = $this->db->query("SELECT * FROM Test",null, "TableTest");
        $this->assertEquals("blabla",$res[0]->getCol1());
        $this->assertEquals("anotherBlabla",$res[1]->getCol1());
    }

    /**
     * Test the execution of file
     */
    public function testExecFile(){
        $this->assertTrue($this->db->executeFile(__DIR__."/databaseTest.pgsql"));
    }

    /**
     * Test Query on dual
     */
    public function testPDOQuery(){
        $res = $this->db->query('SELECT \'Valeur\' as "Valeur"');
        $this->assertFalse(empty($res));
        $this->assertEquals(1,sizeof($res));
        $this->assertEquals("Valeur",$res[0]['Valeur']);
        $res = $this->db->query('SELECT 1 as id, \'anotherBlabla\' as col1 WHERE 1 = :blabla',["blabla"=>1],"TableTest",false);
        $this->assertInstanceOf(PDOStatement::class,$res);
        $this->assertEquals(2,$res->columnCount());
        $obj = $res->fetch();
        $this->assertEquals(1,$obj->getID());
        $this->assertEquals("anotherBlabla",$obj->getCol1());
        $res->closeCursor();
    }
    
    /**
     * Test Commit and Rollback
     */

    public function testPDORollbackExec(){
        $this->assertTrue($this->db->exec("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)"));
        $this->assertTrue($this->db->beginTransaction());
        $this->assertTrue($this->db->exec("INSERT INTO Test(col1) VALUES ('blabla')"));
        $this->assertTrue($this->db->rollback());
        $this->assertTrue(empty($this->db->query("SELECT * FROM Test",null, "TableTest")));
        $this->db->exec("DROP TABLE Test");
    }

    public function testPDOCommitExec(){
        $this->assertTrue($this->db->exec("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)"));
        $this->assertTrue($this->db->beginTransaction());
        $this->assertTrue($this->db->exec("INSERT INTO Test(col1) VALUES ('blabla')"));
        $this->assertTrue($this->db->commit());
        $res = $this->db->query("SELECT * FROM Test",null, "TableTest");
        $this->assertEquals(1,$res[0]->getID());
        $this->assertEquals("blabla",$res[0]->getCol1());
        $this->db->exec("DROP TABLE Test");
    }

    /**
     * Not A SELECT Exception throwing
     */
    public function testExceptionCREATENotASELECT(){
        $this->expectException(Exception::class);
        $this->assertTrue($this->db->beginTransaction());
        $this->db->query("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)");
    }
    public function testExceptionDELETENotASELECT(){
        $this->expectException(Exception::class);
        $this->db->query("DELETE FROM Test *");
    }
    public function testExceptionUPDATENotASELECT(){
        $this->expectException(Exception::class);
        $this->db->query("UPDATE Test SET col1 = 'ttt'");
    }
    public function testExceptionINSERTNotASELECT(){
        $this->expectException(Exception::class);
        $this->db->query("INSERT INTO Test(col1) VALUES ('blabla')");
    }
    public function testExceptionDROPNotASELECT(){
        $this->expectException(Exception::class);
        $this->db->query("DROP TABLE IF EXISTS Test");
    }
    public function testExceptionALTERNotASELECT(){
        $this->expectException(Exception::class);
        $this->db->query("ALTER TABLE Test ADD col2 INT(6)");
    }

    public function testExceptionSELECTNotASELECT(){
        $this->expectException(Exception::class);
        $this->db->exec("SELECT * FROM Test");
    }

    /**
     * isAlter
     */
    public function testExceptionPDOTransactionCREATE(){
        $this->expectException(Exception::class);
        $this->assertTrue($this->db->beginTransaction());
        $this->db->query("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)");
    }

    public function testExceptionPDOTransactionALTER(){
        $this->expectException(Exception::class);
        $this->db->query("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)");
        $this->assertTrue($this->db->beginTransaction());
        $this->db->exec("ALTER TABLE Test ADD col2 INT(6)");   
    }

    public function testExceptionPDOTransactionDROP(){
        $this->expectException(Exception::class);
        $this->db->query("CREATE TABLE IF NOT EXISTS Test (id SERIAL PRIMARY KEY,col1 VARCHAR(30) NOT NULL)");
        $this->assertTrue($this->db->beginTransaction());
        $this->db->exec("DROP TABLE Test");
        $this->assertTrue($this->db->commit());
    }

}

?>