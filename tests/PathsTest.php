<?php

/**
 * This class will test if paths exists
 */
class PathsTest extends TestPageCase
{
    /**
     * test l'index de la page
     */
    public function testIndex(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/");
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testLogin(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/login");
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testRegister(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/register");
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testTournaments(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/tournaments");
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testTournament(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/tournament/".date("Y")); //abstract of tournament
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testMatch(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/tournament/".date("Y")+"/match/1"); // page of match
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testTeams(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/teams"); // page with all teams
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testTeam(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/team/france"); //team reachable by nation name
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testPlayers(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/players"); //player list
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function testPlayer(){
        $this->markTestIncomplete();
        $contents = $this->callPage("GET","/player/Jonathan.Sexton"); //playername is reachable by his name
        $this->assertEquals(200,http_response_code());
        $this->assertNotEmpty($contents);
    }

    public function test404(){
        $contents = $this->callPage("GET","/my_random_page");
        $this->assertNotEmpty($contents);
        $this->assertStringContainsString("Page introuvable",$contents);
    }

    public function test403(){
        $contents = $this->callPage("GET","/admin");
        $this->assertNotEmpty($contents);
        $this->assertStringContainsString('refus',$contents);
        
    }

}
?>