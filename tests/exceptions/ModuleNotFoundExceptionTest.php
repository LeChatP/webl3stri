<?php

use PHPUnit\Framework\TestCase;
use Exceptions\ModuleNotFoundException;

class ModuleNotFoundExceptionTest extends TestCase{

    public function testMessageNull(){
        $exception = new ModuleNotFoundException();
        $this->assertEquals("Le module  est inexistant",$exception->getMessage());
    }

    public function testMessage(){
        $exception = new ModuleNotFoundException(null,"hello");
        $this->assertEquals("Le module hello est inexistant",$exception->getMessage());
    }

    public function testModuleNull(){
        $exception = new ModuleNotFoundException();
        $this->assertNull($exception->getModulename());
    }

    public function testModule(){
        $exception = new ModuleNotFoundException(null,"hello");
        $this->assertEquals("hello",$exception->getModulename());
    }
}
?>