<?php

use PHPUnit\Framework\TestCase;
use Exceptions\FileNotFoundException;

class FileNotFoundExceptionTest extends TestCase{
    public function testTimestamp()
    {
        $time = new DateTime();
        $a=$time->getTimestamp();
        $exception = new FileNotFoundException();
        $b=$time->getTimestamp();
        $this->assertLessThanOrEqual($b,$exception->getTimestamp());
        $this->assertGreaterThanOrEqual($a,$exception->getTimestamp());
    }

    public function testMessageNull(){
        $exception = new FileNotFoundException();
        $this->assertEquals("Le fichier  est inexistant",$exception->getMessage());
    }

    public function testMessage(){
        $exception = new FileNotFoundException(null,"/hello");
        $this->assertEquals("Le fichier /hello est inexistant",$exception->getMessage());
    }

    public function testFileNull(){
        $exception = new FileNotFoundException();
        $this->assertNull($exception->getFilename());
    }

    public function testFile(){
        $exception = new FileNotFoundException(null,"/hello");
        $this->assertEquals("/hello",$exception->getFilename());
    }
}

?>