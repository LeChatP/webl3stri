<?php

//recherche des classes dans le dossier tests
function tests_auto_loader($class)
{
    if(file_exists('tests/' . $class . '.php')){
        require 'tests/' . $class . '.php';
    }
}


// enregistre les fonctions de recherche d'imports
spl_autoload_register('tests_auto_loader');
require_once(__DIR__.'/../src/Autoloader.php');

?>