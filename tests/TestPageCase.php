<?php
use PHPUnit\Framework\TestCase;
use Database\DatabaseHandler;

class TestPageCase extends TestCase
{
    private static $db;

    public static function setUpBeforeClass(): void
    {
        self::$db = DatabaseHandler::getDB();
    }

    public static function tearDownAfterClass(): void
    {
        self::$db = null;
    }

    public function addTestAdminDB(){
        if( empty($userid=self::$db->query("SELECT idu FROM Utilisateur WHERE nomu = 'testAdmin'"))){
            return self::$db->query("INSERT INTO Utilisateur(nomu,mdp,admin) VALUES ('testAdmin','test',true) RETURNING idu")[0]['idu'];
        }else{
            return $userid[0]['idu'];
        }
    }

    public function addTestMemberDB(){
        if( empty($userid=self::$db->query("SELECT idu FROM Utilisateur WHERE nomu = 'testUser'"))){
            return self::$db->query("INSERT INTO Utilisateur(nomu,mdp,admin) VALUES ('testUser','test',false) RETURNING idu")[0]['idu'];
        }else{
            return $userid[0]['idu'];
        }
    }

    public function assertHTMLCommon($xpath):void{
        $this->assertNotFalse($xpath->query("//html")); // <html> </html>
        $this->assertNotFalse($xpath->query("//head"));
        $this->assertNotFalse($xpath->query("//body"));
        $this->assertNotFalse($xpath->query("//title"));
    }
    
    public function callPage(string $type,string $path):string{
        //include 'src/index.php';
        return "HEY Page introuvable refus";
    }

    public function callPageAsTestMember(string $type,string $path){
        Security::checkSession();
        $_SESSION['idUser']=$this->addTestMemberDB();
        return $this->callPage($type,$path);
    }

    public function callPageAsTestAdmin(string $type,string $path){
        Security::checkSession();
        $_SESSION['idUser']=$this->addTestAdminDB();
        return $this->callPage($type,$path);
    }
}

?>