<?php

use PHPUnit\Framework\TestCase;
use Models\UserService;
use Contents\Utilisateur;
use Router\AccessLevel;

class UserServiceTest extends TestCase{

    /**
     * @var int
     */
    private $id;
    /**
     * @var UserService
     */
    private $userservice;
    /**
     * @var Utilisateur
     */
    private $user;

    public function setUp(){
        $this->userservice = new UserService(0);
    }

    public function tearDown(){
        $this->userservice = null;
    }
    public function testGETUser(){
        $this->assertNull($this->userservice->getUtilisateur());
        $this->id = $this->userservice->createUser("testUserService","mdpUserService",null);
        $this->assertIsInt($this->id);
        $this->assertGreaterThan(0,$this->id);
        $this->userservice = new UserService($this->id);
        $this->user = $this->userservice->getUtilisateur();
        $this->assertInstanceOf(Utilisateur::class,$this->user);
        $this->assertEquals($this->id,$this->user->getId());
        $this->assertEquals("testUserService",$this->user->getNom());
        $this->assertEquals("mdpUserService",$this->user->getPassword());
        $this->assertNull($this->user->__get("photo"));
        $this->assertInstanceOf(AccessLevel::class,$this->user->getAccessLevel());
        $this->assertEquals(AccessLevel::MEMBER,$this->user->getAccessLevel()->getLevel());
        $this->assertRegExp('/[0-9]{4}-[0-9]{2}-[0-9]{2}/',$this->user->getInscription());
        $this->userservice->deleteUser($this->id);
    }
}
?>