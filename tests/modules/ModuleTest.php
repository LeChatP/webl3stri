<?php

use PHPUnit\Framework\TestCase;
use Router\AccessLevel;
use Router\Router;
use Router\Renderer;
use Router\Assets;
use Modules\Module;
use Modules\TestModule;

class ModuleTest extends TestCase{
    /**
     * @var TestModule
     */
    private $module;
    /**
     * @var Route
     */
    private $router;
    /**
     * @var Renderer
     */
    private $renderer;
    /**
     * @var AccessLevel
     */
    private $user;

    public function setUp(){
        $this->user = new AccessLevel();
        $this->module = new TestModule($this->router = new Router("",[$this,'null'],[$this,'null']),$this->renderer = new Renderer("",$this->router,new Assets(),$this->user,[]),$this->user);
    }

    public function tearDown()
    {
        unset($this->module);
    }
    
    public function null(){
    }

    public function testModuleGetters(){
        $this->assertSame($this->router,$this->module->getRouter());
        $this->assertSame($this->renderer,$this->module->getRenderer());
        $this->assertInstanceOf(AccessLevel::class,$this->module->getAccessLevel());
    }

    public function testIsLogged(){
        $this->assertFalse($this->module->is_logged());
    }

    public function testIsLoggedAsMember(){
        $this->user = new AccessLevel(AccessLevel::MEMBER);
        $this->module = new TestModule($this->router = new Router("",[$this,'null'],[$this,'null']),$this->renderer = new Renderer("",$this->router,new Assets(),$this->user,[]),$this->user);
        $this->assertTrue($this->module->is_logged());
    }

    public function testIsAdmin(){
        $this->assertFalse($this->module->is_admin());
    }

    public function testIsAdminAsAdmin(){
        $this->user = new AccessLevel(AccessLevel::ADMIN);
        $this->module = new TestModule($this->router = new Router("",[$this,'null'],[$this,'null']),$this->renderer = new Renderer("",$this->router,new Assets(),$this->user,[]),$this->user);
        $this->assertTrue($this->module->is_admin());
    }
}
?>