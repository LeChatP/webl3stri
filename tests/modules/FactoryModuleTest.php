<?php

use PHPUnit\Framework\TestCase;
use Modules\FactoryModule;
use Router\AccessLevel;
use Router\Router;
use Router\Renderer;
use Router\Assets;
use Exceptions\ModuleNotFoundException;
/**
 * @covers Modules\FactoryModule
 * @covers Modules\Module
 */
class FactoryModuleTest extends TestCase {
    /**
     * @var FactoryModule
     */
    private $factory;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var Renderer
     */
    private $renderer;
    /**
     * @var 
     */
    private $access;

    public function setUp(){
        $this->access = new AccessLevel();
        $this->factory = new FactoryModule(
            $this->router = new Router('/',[$this,'null'],[$this,'null']),
            $this->renderer = new Renderer("",$this->router,new Assets(),$this->access,[]),
            $this->access);
    }


    public function testModule(){
        $module = $this->factory->getModule('Test');
        $this->assertNotNull($module);
        $this->assertInstanceOf('Modules\Module',$module);
        $this->assertInstanceOf('Modules\TestModule',$module);
        $this->assertEquals(str_replace("tests","src",__DIR__),$module->getDir());
    }

    /**
     * Teste la récupération d'url POST a partir du nom du chemin
     */
    public function testUnknownModule(){
        $this->expectException(ModuleNotFoundException::class);
        $module = $this->factory->getModule('TestUnknown');
    }

    public function null(){
        return;
    }
}
?>