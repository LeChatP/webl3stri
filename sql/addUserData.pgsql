INSERT INTO Utilisateur(nomU,mdp,admin) VALUES
('ebilloir','HASH256',true),
('test1','HASH256',false),
('test2','HASH256',false);

INSERT INTO Commenter(idMatch,idU,texte) VALUES
(1,2,'Super ce match'),
(2,3,'Un beau commentaire'),
(3,2,'Woaw');

-- Un utilisateur pari sur le pays de galles / italie
INSERT INTO Score(essai, transformation, penalite, numEq) VALUES
('3','1','0','6'), -- id 31 Pari de Pays de Galles
('1','2','3','5'); -- id 32 Pari de Italie

INSERT INTO Parier(idU,idScore,idMatch) VALUES
('2','31','1'), -- test1 pari que le pays de galles va effectuer un score sur le match Pays de galles/Italie
('2','32','1'); -- test1 pari que l'italie va effectuer un score sur le match Pays de galles/Italie