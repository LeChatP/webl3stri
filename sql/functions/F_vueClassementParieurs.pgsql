CREATE OR REPLACE VIEW classementParieur AS
SELECT p.*, calculPointsPari(p.idMatch,p.idu) as Points
FROM Paris p, resultat r
WHERE r.idMatch = p.idMatch AND
      r.scorea IS NOT NULL AND
      r.scoreb IS NOT NULL
ORDER BY Points DESC;