CREATE VIEW Paris AS
  SELECT DISTINCT
   p.idU,
   p.idMatch,
   p.datePari,
   e1.numEq as EquipeA,
   calculScore(s1.essai,s1.transformation,s1.penalite) as ScoreA,
   s1.essai as EssaiA,
   s1.transformation as TransformationA,
   s1.penalite as PenaliteA,
   e2.numEq as EquipeB,
   calculScore(s2.essai,s2.transformation,s2.penalite) as ScoreB,
   s2.essai as EssaiB,
   s2.transformation as TransformationB,
   s2.penalite as PenaliteB
   FROM Parier p CROSS join Parier p1, Equipe e1, Equipe e2, Score s1, Score s2
   WHERE p.idScore = s1.idScore AND s1.numEq = e1.numEq
   AND p.idMatch = p1.idMatch AND p1.idScore != p.idScore AND p1.idScore = s2.idScore AND s2.numEq = e2.numEq
   AND s1.idScore > s2.idScore;