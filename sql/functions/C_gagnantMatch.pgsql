-- obtient le gagnant du match
CREATE OR REPLACE FUNCTION gagnantMatch(in idMatch integer)
  RETURNS INTEGER
  LANGUAGE plpgsql
AS $GM$
DECLARE numeq INTEGER;
BEGIN
  SELECT CASE 
            WHEN r.scorea = NULL then NULL
            WHEN r.scoreb = NULL then NULL
            WHEN r.scorea > r.scoreb then r.equipea
            WHEN r.scoreb > r.scorea then r.equipeb
            ELSE 0
         END AS numeq FROM resultat r where r.idMatch = idMatch;
  return numeq;
END
$GM$ RETURNS NULL ON NULL INPUT;
