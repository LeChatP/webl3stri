--Matchs nuls d'une équipe
CREATE OR REPLACE FUNCTION matchNuls(in equipe integer)
  RETURNS INTEGER
  LANGUAGE plpgsql
AS $MN$
DECLARE nb INTEGER;
BEGIN
  SELECT COUNT(*) INTO nb
  FROM resultat r
  WHERE r.ScoreA = r.ScoreB
    AND (r.EquipeA = equipe OR r.EquipeB = equipe);
  RETURN nb;
END
$MN$ RETURNS NULL ON NULL INPUT;