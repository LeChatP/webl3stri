CREATE OR REPLACE VIEW classement AS
SELECT e.pays as Pays, (matchGagnes(e.numEq)*4 + matchNuls(e.numEq)*2) AS Points, matchGagnes(e.numEq) as Wins, matchNuls(e.numEq) as Nuls, matchPerdus(e.numEq) as Perdus
FROM Equipe e
ORDER BY Points DESC;