CREATE OR REPLACE FUNCTION calculPointsPari(idMatch INTEGER, idU INTEGER)
  RETURNS INTEGER
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE nb INTEGER;
BEGIN
  SELECT COUNT(*) INTO nb FROM Parier p, resultat r, Score s
            WHERE p.idMatch = r.idMatch
            AND p.idMatch = idMatch
            AND p.idU = idU
            AND (s.numEq = r.equipea 
            AND s.essai = r.essaia
            AND s.transformation = r.transformationa
            AND s.penalite = r.penalitea
            ) OR (s.numEq = r.equipeb 
            AND s.essai = r.essaib
            AND s.transformation = r.transformationb
            AND s.penalite = r.penaliteb);
  IF nb = 2 THEN
    return 300;
  END IF;
  SELECT COUNT(*) INTO nb FROM Parier p, resultat r, Score s
            WHERE p.idMatch = r.idMatch
            AND p.idMatch = idMatch
            AND p.idU = idU
            AND p.idScore = s.idScore
            AND (s.numEq = r.equipea AND calculScore(s.essai,s.transformation,s.penalite) = r.scorea) OR 
                (s.numEq = r.equipeb AND calculScore(s.essai,s.transformation,s.penalite) = r.scoreb);
  IF nb = 2 THEN
    return 100;
  ELSIF nb = 1 THEN
    return 25;
  END IF;
  SELECT COUNT(*) INTO nb FROM Parier p, resultat r, Score s
              WHERE p.idMatch = r.idMatch
              AND p.idMatch = idMatch
              AND p.idU = idU
              AND p.idScore = s.idScore
              AND (s.numEq = gagnantMatch(idMatch) 
                  AND calculScore(s.essai,s.transformation,s.penalite) >= r.scorea);
  IF nb > 0 THEN
    return 10;
  END IF;
  return -25;
END
$$;