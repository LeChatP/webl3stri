--Matchs gagnés d'une équipe
CREATE OR REPLACE FUNCTION calculScore(in essais integer,in transformations INTEGER,in penalites INTEGER)
  RETURNS INTEGER
  LANGUAGE plpgsql
AS $CS$
DECLARE nb INTEGER;
BEGIN
  return (essais*5+transformations*3+penalites*2);
END
$CS$ RETURNS NULL ON NULL INPUT;