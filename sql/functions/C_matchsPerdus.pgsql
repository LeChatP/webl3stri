--Matchs perdus d'une équipe
CREATE OR REPLACE FUNCTION matchPerdus(in equipe integer)
  RETURNS INTEGER
  LANGUAGE plpgsql
AS $MP$
DECLARE nb INTEGER;
BEGIN
  SELECT COUNT(*) INTO nb
  FROM resultat r
  WHERE r.ScoreA < r.ScoreB
    AND r.EquipeA = equipe
    OR r.ScoreB < r.ScoreA
    AND r.EquipeB = equipe;
  RETURN nb;
END
$MP$ RETURNS NULL ON NULL INPUT;