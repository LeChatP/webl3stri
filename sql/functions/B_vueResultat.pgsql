--Calcul des résultats de tous les matchs
CREATE OR REPLACE VIEW resultat AS
SELECT m.idMatch,
e1.numEq as equipea, e1.pays as paysa,calculScore(s1.essai,s1.transformation,s1.penalite) as scorea, 
s1.essai as EssaiA, s1.transformation as TransformationA, s1.penalite as PenaliteA,
e2.numEq as equipeb,e2.pays as paysb,calculScore(s2.essai,s2.transformation,s2.penalite) as scoreb,
s2.essai as EssaiB, s2.transformation as TransformationB, s2.penalite as PenaliteB
FROM eMatch m, Score s1,Score s2, Equipe e1, Equipe e2
WHERE s1.numEq = e1.numEq
   AND s2.numEq = e2.numEq
   AND m.idscore_equipea = s1.idScore
   AND m.idscore_equipeb = s2.idScore;