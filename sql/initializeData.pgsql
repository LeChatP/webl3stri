INSERT INTO Equipe(pays) VALUES ('Angleterre'), ('Écosse'), ('France'), ('Irlande'), ('Italie'), ('Pays de Galles');

-- joueurs d'angleterre 2020
INSERT INTO Joueur(prenom,nom,naissance,taille,poids,numEq) VALUES
('Luke','Cowan-Dickie','01/01/1994',1.84,112,1),
('Thomas','Curry','01/01/1999',1.85,102,1),
('Elliot','Daly','01/01/1993',1.83,98,1),
('Ollie','Devoto','01/01/1994',1.93,102,1),
('Fraser','Dingwall','01/01/2000',1.88,86,1),
('Tom','Dunn','01/01/1993',1.87,106,1),
('Ben','Earl','01/01/1998',1.83,101,1),
('Charlie','Ewels','01/01/1996',1.97,112,1),
('Owen','Farrell','01/01/1992',1.86,96,1),
('George','Ford','01/01/1993',1.78,86,1),
('George','Furbank',NULL,NULL,NULL,1),
('Ellis','Genge','01/01/1995',1.88,116,1),
('Jamie','George','01/01/1991',1.83,113,1),
('Willi','Heinz','01/01/1987',1.8,89,1),
('Ted','Hill','01/01/2000',1.96,112,1),
('Maro','Itoje','01/01/1995',1.95,116,1),
('Jonathan','Joseph','01/01/1992',1.83,91,1),
('George','Kruis','01/01/1990',1.98,118,1),
('Joe','Launchbury','01/01/1992',1.98,126,1),
('Courtney','Lawes','01/01/1989',2.01,115,1),
('Lewis','Ludlam','01/01/1996',1.9,110,1),
('Joe','Marler','01/01/1991',1.83,114,1),
('Jonny','May','01/01/1991',1.88,90,1),
('Alexander','Moon',NULL,2.07,123,1),
('Beno','Obano',NULL,1.73,120,1),
('Kyle','Sinckler','01/01/1994',1.8,122,1),
('Henry','Slade','01/01/1994',1.87,83,1),
('Will','Stuart','01/01/1997',1.89,122,1),
('Ollie','Thorley',NULL,1.85,97,1),
('Manu','Tuilagi','01/01/1992',1.83,110,1),
('Jacob','Umaga','01/01/1999',1.82,90,1),
('Sam','Underhill','01/01/1997',1.86,103,1),
('Mako','Vunipola','01/01/1991',1.8,121,1),
('Anthony','Watson','01/01/1994',1.85,93,1),
('Harry','Williams','01/01/1992',1.91,131,1),
('Ben','Youngs','01/01/1990',1.78,88,1);

-- joueurs d'ecosse 2020
INSERT INTO Joueur(prenom,nom,naissance,taille,poids,numEq) VALUES
('Simon','Berghan','01/01/1991',1.91,120,2),
('Jamie','Bhatti','01/01/1994',1.83,113,2),
('Magnus','Bradbury','01/01/1996',1.93,110,2),
('Fraser','Brown','01/01/1990',1.86,103,2),
('Alex','Craig',NULL,1.96,105,2),
('Luke','Crosbie',NULL,1.93,108,2),
('Scott','Cummings','01/01/1997',1.98,116,2),
('Allan','Dell','01/01/1992',1.85,108,2),
('Cornell','du Preez','01/01/1992',1.92,117,2),
('Zander','Fagerson','01/01/1996',1.88,114,2),
('Matt','Fagerson','01/01/1998',1.86,98,2),
('Grant','Gilchrist','01/01/1991',1.98,118,2),
('Thomas','Gordon',NULL,1.8,98,2),
('Darcy','Graham','01/01/1998',1.78,84,2),
('Jonny','Gray','01/01/1994',1.98,119,2),
('Nick','Haining','01/01/1991',1.93,114,2),
('Chris','Harris','01/01/1991',1.88,104,2),
('Adam','Hastings','01/01/1997',1.85,89,2),
('Stuart','Hogg','01/01/1993',1.83,88,2),
('George','Horne','01/01/1996',1.75,80,2),
('Rory','Hutchinson','01/01/1996',1.81,95,2),
('Sam','Johnson','01/01/1994',1.8,93,2),
('Huw','Jones','01/01/1994',1.85,96,2),
('Blair','Kinghorn','01/01/1997',1.91,105,2),
('Sean','Maitland','01/01/1989',1.88,100,2),
('Byron','McGuigan','01/01/1990',1.83,110,2),
('Stuart','McInally','01/01/1991',1.91,110,2),
('WP','Nel','01/01/1987',1.8,115,2),
('Ali','Price','01/01/1994',1.78,80,2),
('Henry','Pyrgos','01/01/1990',1.78,83,2),
('Jamie','Ritchie','01/01/1997',1.92,96,2),
('Finn','Russell','01/01/1993',1.83,86,2),
('Matt','Scott','01/01/1991',1.85,101,2),
('Sam','Skinner','01/01/1995',1.96,111,2),
('Kyle','Steyn','01/01/1994',1.87,97,2),
('Rory','Sutherland','01/01/1993',1.83,110,2),
('Ratu','Tagive','01/01/1992',1.9,102,2),
('Ben','Toolis','01/01/1992',1.95,115,2),
('George','Turner',NULL,1.8,103,2),
('Hamish','Watson','01/01/1992',1.85,91,2),
('Duncan','Weir','01/01/1992',1.72,92,2);

-- joueurs de france 2020
INSERT INTO Joueur(prenom,nom,naissance,taille,poids,numEq) VALUES
('Gregory','Alldritt','01/01/1998',1.91,107,3),
('Uini','Atonio','01/01/1991',1.96,151,3),
('Cyril','Baille','01/01/1994',1.82,115,3),
('Demba','Bamba','01/01/1999',1.85,124,3),
('Teddy','Baubigny','01/01/1999',1.84,109,3),
('Anthony','Bouthier','01/01/1993',1.82,79,3),
('Louis','Carbonel','01/01/1999',1.8,80,3),
('Cyril','Cazeaux','01/01/1995',1.95,107,3),
('Camille','Chat','01/01/1996',1.84,99,3),
('Gervais','Cordin','01/01/1999',1.72,73,3),
('Dylan','Cretin','01/01/1998',1.88,104,3),
('Francois','Cros','01/01/1995',1.9,97,3),
('Baptiste','Delaporte','01/01/1998',1.9,94,3),
('Guillaume','Ducat','01/01/1997',2.05,110,3),
('Antoine','Dupont','01/01/1997',1.75,82,3),
('Lester','Etien','01/01/1996',1.8,90,3),
('Gael','Fickou','01/01/1995',1.91,100,3),
('Alexandre','Fischer',NULL,NULL,NULL,3),
('Killian','Geraci','01/01/2000',1.99,106,3),
('Jean-Baptiste','Gros','01/01/2000',1.8,80,3),
('Kylan','Hamdaoui','01/01/1995',1.82,84,3),
('Mohamed','Haouas','01/01/1994',1.85,118,3),
('Julien','Heriteau','01/01/1995',1.82,88,3),
('Matthieu','Jalibert','01/01/1999',1.8,79,3),
('Anthony','Jelonch','01/01/1997',1.93,105,3),
('Bernard','le Roux','01/01/1990',1.97,112,3),
('Maxime','Lucu','01/01/1993',1.77,79,3),
('Sekou','Macalou','01/01/1996',1.95,100,3),
('Julien','Marchand','01/01/1996',1.8,95,3),
('Peato','Mauvaka','01/01/1997',1.84,121,3),
('Gabriel','Ngandebe',NULL,1.73,74,3),
('Romain','Ntamack','01/01/2000',1.85,84,3),
('Charles','Ollivon','01/01/1994',1.99,108,3),
('Boris','Palu','01/01/1996',1.93,102,3),
('Damian','Penaud','01/01/1997',1.93,94,3),
('Jefferson','Poirot','01/01/1993',1.8,112,3),
('Thomas','Ramos','01/01/1996',1.78,86,3),
('Vincent','Rattez','01/01/1993',1.75,83,3),
('Yvan','Reilhac','01/01/1996',1.82,92,3),
('Alexandre','Roumat',NULL,1.98,91,3),
('Baptiste','Serin','01/01/1995',1.8,79,3),
('Romain','Taofifenua','01/01/1991',2,133,3),
('Lucas','Tauzin','01/01/1999',1.87,98,3),
('Teddy','Thomas','01/01/1994',1.85,90,3),
('Selevasio','Tolofua','01/01/1998',1.86,100,3),
('Virimi','Vakatawa','01/01/1993',1.85,92,3),
('Arthur','Vincent','01/01/2000',1.83,90,3),
('Paul','Willemse','01/01/1993',2.01,136,3),
('Cameron','Woki','01/01/1999',1.93,103,3);

-- joueurs d'irlande 2020
INSERT INTO Joueur(prenom,nom,naissance,taille,poids,numEq) VALUES
('Will','Addison',NULL,1.85,94,4),
('Bundee','Aki','01/01/1991',1.83,101,4),
('Billy','Burns',NULL,1.8,85,4),
('Ross','Byrne','01/01/1996',1.88,91,4),
('Will','Connors','01/01/1997',1.93,102,4),
('Andrew','Conway','01/01/1992',1.78,82,4),
('John','Cooney','01/01/1991',1.78,87,4),
('Max','Deegan','01/01/1997',1.93,108,4),
('Ultan','Dillane','01/01/1994',1.98,115,4),
('Caelan','Doris','01/01/1999',1.93,106,4),
('Keith','Earls','01/01/1988',1.8,90,4),
('Chris','Farrell','01/01/1993',1.91,110,4),
('Tadhg','Furlong','01/01/1993',1.85,126,4),
('Cian','Healy','01/01/1988',1.85,115,4),
('Dave','Heffernan',NULL,1.85,101,4),
('Iain','Henderson','01/01/1992',1.98,117,4),
('Robbie','Henshaw','01/01/1994',1.91,103,4),
('Rob','Herring','01/01/1991',1.88,103,4),
('David','Kearney','01/01/1990',1.81,95,4),
('Ronan','Kelleher','01/01/1998',1.83,104,4),
('David','Kilcoyne','01/01/1989',1.83,110,4),
('Jordan','Larmour','01/01/1998',1.78,91,4),
('Luke','McGrath','01/01/1993',1.75,84,4),
('Jack','McGrath','01/01/1990',1.85,118,4),
('Conor','Murray','01/01/1990',1.88,93,4),
('Jack','O''Donoghue',NULL,1.88,97,4),
('Peter','O''Mahony','01/01/1990',1.91,107,4),
('Tom','O''Toole','01/01/1999',1.88,113,4),
('Andrew','Porter','01/01/1996',1.83,120,4),
('Garry','Ringrose','01/01/1995',1.85,94,4),
('James','Ryan','01/01/1997',2.03,107,4),
('Jonathan','Sexton','01/01/1986',1.88,92,4),
('CJ','Stander','01/01/1991',1.85,114,4),
('Jacob','Stockdale','01/01/1997',1.91,103,4),
('Devin','Toner','01/01/1987',2.08,124,4),
('Josh','van der Flier','01/01/1994',1.85,104,4);

-- joueurs d'italie 2020
INSERT INTO Joueur(prenom,nom,naissance,taille,poids,numEq) VALUES
('Tommaso','Allan','01/01/1994',1.84,87,5),
('Mattia','Bellini','01/01/1994',1.93,93,5),
('Tommaso','Benvenuti','01/01/1991',1.86,93,5),
('Luca','Bigi','01/01/1992',1.8,87,5),
('Michelangelo','Biondelli',NULL,NULL,NULL,5),
('Giulio','Bisegni','01/01/1993',1.8,83,5),
('Tommaso','Boni','01/01/1993',1.86,99,5),
('Callum','Braley','01/01/1995',1.78,83,5),
('Dean','Budd','01/01/1987',1.96,108,5),
('Carlo','Canna','01/01/1993',1.91,92,5),
('Niccolo','Cannone',NULL,NULL,NULL,5),
('Pietro','Ceccarelli','01/01/1992',1.84,118,5),
('Oliviero','Fabiani','01/01/1991',1.85,98,5),
('Danilo','Fischetti',NULL,NULL,NULL,5),
('Jayden','Hayward','01/01/1987',1.85,92,5),
('Marco','Lazzaroni','01/01/1996',1.91,100,5),
('Giovanni','Licata','01/01/1997',1.95,106,5),
('Andrea','Lovotti','01/01/1990',1.85,108,5),
('Johan','Meyer','01/01/1993',1.93,104,5),
('Matteo','Minozzi','01/01/1997',1.78,77,5),
('Luca','Morisi','01/01/1991',1.85,97,5),
('Sebastian','Negri','01/01/1995',1.96,108,5),
('Edoardo','Padovani','01/01/1994',1.91,100,5),
('Guglielmo','Palazzani','01/01/1992',1.73,87,5),
('Jake','Polledri','01/01/1996',1.89,106,5),
('Marco','Riccioni','01/01/1998',1.83,116,5),
('Federico','Ruzza','01/01/1995',1.98,108,5),
('Leonardo','Sarto','01/01/1992',1.93,93,5),
('Alberto','Sgarbi','01/01/1987',1.92,101,5),
('Dave','Sisi','01/01/1993',1.96,122,5),
('Abraham','Jurgen Steyn','01/01/1993',1.93,110,5),
('Jimmy','Tuivaiti','01/01/1988',1.83,113,5),
('Marcello','Violi','01/01/1994',1.76,80,5),
('Federico','Zani','01/01/1990',1.8,115,5),
('Alessandro','Zanni','01/01/1984',1.93,106,5),
('Giosue','Zilocchi','01/01/1997',1.88,112,5);

-- joueurs de l'équipe du pays de galle
INSERT INTO Joueur(prenom,nom,naissance,taille,poids,numEq) VALUES
('Josh','Adams','01/01/1996',1.88,94,6),
('Jake','Ball','01/01/1992',1.97,121,6),
('Adam','Beard','01/01/1996',2.03,117,6),
('Dan','Biggar','01/01/1990',1.88,89,6),
('Leon','Brown','01/01/1997',1.88,126,6),
('Rhys','Carre','01/01/1998',1.89,116,6),
('Gareth','Davies','01/01/1991',1.78,88,6),
('Seb','Davies','01/01/1997',1.98,116,6),
('Elliot','Dee',NULL,1.82,104,6),
('Ryan','Elias','01/01/1995',1.85,104,6),
('Rob','Evans','01/01/1993',1.85,118,6),
('Jarrod','Evans','01/01/1997',1.78,86,6),
('Taulupe','Faletau','01/01/1991',1.87,111,6),
('Leigh','Halfpenny','01/01/1989',1.78,85,6),
('Cory','Hill','01/01/1992',1.95,111,6),
('Jonah','Holmes','01/01/1993',1.83,91,6),
('WillGriff','John','01/01/1993',1.88,120,6),
('Alun','Wyn Jones','01/01/1986',1.98,118,6),
('Wyn','Jones','01/01/1992',1.84,114,6),
('Owen','Lane','01/01/1998',1.85,100,6),
('Dillon','Lewis','01/01/1996',1.83,120,6),
('Johnny','McNicholl','01/01/1991',1.85,96,6),
('Ross','Moriarty','01/01/1995',1.88,103,6),
('Josh','Navidi','01/01/1991',1.85,95,6),
('George','North','01/01/1993',1.93,109,6),
('Ken','Owens','01/01/1987',1.85,109,6),
('Hadleigh','Parkes','01/01/1988',1.87,101,6),
('Louis','Rees-Zammit',NULL,NULL,NULL,6),
('Will','Rowlands','01/01/1992',2.03,120,6),
('Aaron','Shingler','01/01/1988',1.97,105,6),
('Justin','Tipuric','01/01/1990',1.88,102,6),
('Nick','Tompkins','01/01/1995',1.83,94,6),
('Aaron','Wainwright','01/01/1998',1.89,105,6),
('Owen','Watkin',NULL,1.91,103,6),
('Rhys','Webb','01/01/1989',1.83,93,6),
('Owen','Williams','01/01/1992',1.83,92,6),
('Liam','Williams','01/01/1992',1.85,87,6),
('Tomos','Williams','01/01/1995',1.78,77,6);

UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/99187.jpg' WHERE prenom = 'Luke' AND nom = 'Cowan-Dickie';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/127962.jpg' WHERE prenom = 'Thomas' AND nom = 'Curry';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/96542.jpg' WHERE prenom = 'Elliot' AND nom = 'Daly';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/106794.jpg' WHERE prenom = 'Ollie' AND nom = 'Devoto';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/128174.jpg' WHERE prenom = 'Fraser' AND nom = 'Dingwall';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/109124.jpg' WHERE prenom = 'Tom' AND nom = 'Dunn';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/131065.jpg' WHERE prenom = 'Ben' AND nom = 'Earl';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/115614.jpg' WHERE prenom = 'Charlie' AND nom = 'Ewels';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/97969.jpg' WHERE prenom = 'Owen' AND nom = 'Farrell';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/88441.jpg' WHERE prenom = 'George' AND nom = 'Ford';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/116012.jpg' WHERE prenom = 'George' AND nom = 'Furbank';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/116237.jpg' WHERE prenom = 'Ellis' AND nom = 'Genge';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/73525.jpg' WHERE prenom = 'Jamie' AND nom = 'George';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/122963.jpg' WHERE prenom = 'Willi' AND nom = 'Heinz';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/128184.jpg' WHERE prenom = 'Ted' AND nom = 'Hill';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/110500.jpg' WHERE prenom = 'Maro' AND nom = 'Itoje';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/88435.jpg' WHERE prenom = 'Jonathan' AND nom = 'Joseph';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/88405.jpg' WHERE prenom = 'George' AND nom = 'Kruis';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/90347.jpg' WHERE prenom = 'Joe' AND nom = 'Launchbury';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/33623.jpg' WHERE prenom = 'Courtney' AND nom = 'Lawes';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/116010.jpg' WHERE prenom = 'Lewis' AND nom = 'Ludlam';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/73532.jpg' WHERE prenom = 'Joe' AND nom = 'Marler';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/77838.jpg' WHERE prenom = 'Jonny' AND nom = 'May';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/123392.jpg' WHERE prenom = 'Alexander' AND nom = 'Moon';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/ENGL6288.svg' WHERE prenom = 'Beno' AND nom = 'Obano';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/102012.jpg' WHERE prenom = 'Kyle' AND nom = 'Sinckler';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/ENGL6288.svg' WHERE prenom = 'Henry' AND nom = 'Slade';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/124646.jpg' WHERE prenom = 'Will' AND nom = 'Stuart';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/ENGL6288.svg' WHERE prenom = 'Ollie' AND nom = 'Thorley';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/97776.jpg' WHERE prenom = 'Manu' AND nom = 'Tuilagi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/130212.jpg' WHERE prenom = 'Jacob' AND nom = 'Umaga';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/115446.jpg' WHERE prenom = 'Sam' AND nom = 'Underhill';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/99655.jpg' WHERE prenom = 'Mako' AND nom = 'Vunipola';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/98454.jpg' WHERE prenom = 'Anthony' AND nom = 'Watson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/110477.jpg' WHERE prenom = 'Harry' AND nom = 'Williams';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/125/250x250/62787.jpg' WHERE prenom = 'Ben' AND nom = 'Youngs';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/116071.jpg' WHERE prenom = 'Simon' AND nom = 'Berghan';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/105534.jpg' WHERE prenom = 'Jamie' AND nom = 'Bhatti';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/113014.jpg' WHERE prenom = 'Magnus' AND nom = 'Bradbury';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/73050.jpg' WHERE prenom = 'Fraser' AND nom = 'Brown';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/122971.jpg' WHERE prenom = 'Alex' AND nom = 'Craig';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/131377.jpg' WHERE prenom = 'Luke' AND nom = 'Crosbie';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/121903.jpg' WHERE prenom = 'Scott' AND nom = 'Cummings';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/106082.jpg' WHERE prenom = 'Allan' AND nom = 'Dell';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/100940.jpg' WHERE prenom = 'Cornell' AND nom = 'du Preez';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/116404.jpg' WHERE prenom = 'Zander' AND nom = 'Fagerson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/127954.jpg' WHERE prenom = 'Matt' AND nom = 'Fagerson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/84913.jpg' WHERE prenom = 'Grant' AND nom = 'Gilchrist';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/131854.jpg' WHERE prenom = 'Thomas' AND nom = 'Gordon';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/126806.jpg' WHERE prenom = 'Darcy' AND nom = 'Graham';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/105535.jpg' WHERE prenom = 'Jonny' AND nom = 'Gray';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/112214.jpg' WHERE prenom = 'Nick' AND nom = 'Haining';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/106734.jpg' WHERE prenom = 'Chris' AND nom = 'Harris';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/123362.jpg' WHERE prenom = 'Adam' AND nom = 'Hastings';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/99851.jpg' WHERE prenom = 'Stuart' AND nom = 'Hogg';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/115803.jpg' WHERE prenom = 'George' AND nom = 'Horne';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/115890.jpg' WHERE prenom = 'Rory' AND nom = 'Hutchinson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/125029.jpg' WHERE prenom = 'Sam' AND nom = 'Johnson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/127466.jpg' WHERE prenom = 'Huw' AND nom = 'Jones';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/122847.jpg' WHERE prenom = 'Blair' AND nom = 'Kinghorn';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/109050.jpg' WHERE prenom = 'Sean' AND nom = 'Maitland';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/105955.jpg' WHERE prenom = 'Byron' AND nom = 'McGuigan';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/82052.jpg' WHERE prenom = 'Stuart' AND nom = 'McInally';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/106503.jpg' WHERE prenom = 'WP' AND nom = 'Nel';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/103100.jpg' WHERE prenom = 'Ali' AND nom = 'Price';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/79742.jpg' WHERE prenom = 'Henry' AND nom = 'Pyrgos';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/116450.jpg' WHERE prenom = 'Jamie' AND nom = 'Ritchie';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/105532.jpg' WHERE prenom = 'Finn' AND nom = 'Russell';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/95131.jpg' WHERE prenom = 'Matt' AND nom = 'Scott';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/SCOT3017.svg' WHERE prenom = 'Sam' AND nom = 'Skinner';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/143889.jpg' WHERE prenom = 'Kyle' AND nom = 'Steyn';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/114065.jpg' WHERE prenom = 'Rory' AND nom = 'Sutherland';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/131575.jpg' WHERE prenom = 'Ratu' AND nom = 'Tagive';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/114066.jpg' WHERE prenom = 'Ben' AND nom = 'Toolis';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/101144.jpg' WHERE prenom = 'George' AND nom = 'Turner';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/127/250x250/97813.jpg' WHERE prenom = 'Hamish' AND nom = 'Watson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/SCOT3017.svg' WHERE prenom = 'Duncan' AND nom = 'Weir';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/141574.jpg' WHERE prenom = 'Gregory' AND nom = 'Alldritt';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Uini' AND nom = 'Atonio';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/110557.jpg' WHERE prenom = 'Cyril' AND nom = 'Baille';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/135958.jpg' WHERE prenom = 'Demba' AND nom = 'Bamba';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/141593.jpg' WHERE prenom = 'Teddy' AND nom = 'Baubigny';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/147378.jpg' WHERE prenom = 'Anthony' AND nom = 'Bouthier';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/137085.jpg' WHERE prenom = 'Louis' AND nom = 'Carbonel';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/121902.jpg' WHERE prenom = 'Cyril' AND nom = 'Cazeaux';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/116466.jpg' WHERE prenom = 'Camille' AND nom = 'Chat';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/137575.jpg' WHERE prenom = 'Gervais' AND nom = 'Cordin';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/129794.jpg' WHERE prenom = 'Dylan' AND nom = 'Cretin';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/110888.jpg' WHERE prenom = 'Francois' AND nom = 'Cros';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Baptiste' AND nom = 'Delaporte';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Guillaume' AND nom = 'Ducat';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/118521.jpg' WHERE prenom = 'Antoine' AND nom = 'Dupont';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Lester' AND nom = 'Etien';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/106213.jpg' WHERE prenom = 'Gael' AND nom = 'Fickou';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/141850.jpg' WHERE prenom = 'Alexandre' AND nom = 'Fischer';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/131572.jpg' WHERE prenom = 'Killian' AND nom = 'Geraci';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/137562.jpg' WHERE prenom = 'Jean-Baptiste' AND nom = 'Gros';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Kylan' AND nom = 'Hamdaoui';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/135911.jpg' WHERE prenom = 'Mohamed' AND nom = 'Haouas';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/112586.jpg' WHERE prenom = 'Julien' AND nom = 'Heriteau';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/131885.jpg' WHERE prenom = 'Matthieu' AND nom = 'Jalibert';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Anthony' AND nom = 'Jelonch';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/94722.jpg' WHERE prenom = 'Bernard' AND nom = 'le Roux';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/147434.jpg' WHERE prenom = 'Maxime' AND nom = 'Lucu';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/121912.jpg' WHERE prenom = 'Sekou' AND nom = 'Macalou';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/118837.jpg' WHERE prenom = 'Julien' AND nom = 'Marchand';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/126785.jpg' WHERE prenom = 'Peato' AND nom = 'Mauvaka';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/126812.jpg' WHERE prenom = 'Gabriel' AND nom = 'Ngandebe';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/131569.jpg' WHERE prenom = 'Romain' AND nom = 'Ntamack';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/113529.jpg' WHERE prenom = 'Charles' AND nom = 'Ollivon';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/135945.jpg' WHERE prenom = 'Boris' AND nom = 'Palu';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/121897.jpg' WHERE prenom = 'Damian' AND nom = 'Penaud';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/104919.jpg' WHERE prenom = 'Jefferson' AND nom = 'Poirot';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/121917.jpg' WHERE prenom = 'Thomas' AND nom = 'Ramos';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/129703.jpg' WHERE prenom = 'Vincent' AND nom = 'Rattez';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/125664.jpg' WHERE prenom = 'Yvan' AND nom = 'Reilhac';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/127603.jpg' WHERE prenom = 'Alexandre' AND nom = 'Roumat';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/107894.jpg' WHERE prenom = 'Baptiste' AND nom = 'Serin';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/96698.jpg' WHERE prenom = 'Romain' AND nom = 'Taofifenua';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/FRAN5896.svg' WHERE prenom = 'Lucas' AND nom = 'Tauzin';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/107202.jpg' WHERE prenom = 'Teddy' AND nom = 'Thomas';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/130750.jpg' WHERE prenom = 'Selevasio' AND nom = 'Tolofua';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/99268.jpg' WHERE prenom = 'Virimi' AND nom = 'Vakatawa';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/137110.jpg' WHERE prenom = 'Arthur' AND nom = 'Vincent';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/124952.jpg' WHERE prenom = 'Paul' AND nom = 'Willemse';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/129/250x250/131573.jpg' WHERE prenom = 'Cameron' AND nom = 'Woki';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/88878.jpg' WHERE prenom = 'Will' AND nom = 'Addison';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/101718.jpg' WHERE prenom = 'Bundee' AND nom = 'Aki';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/105502.jpg' WHERE prenom = 'Billy' AND nom = 'Burns';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/114843.jpg' WHERE prenom = 'Ross' AND nom = 'Byrne';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/IREL3260.svg' WHERE prenom = 'Will' AND nom = 'Connors';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/88084.jpg' WHERE prenom = 'Andrew' AND nom = 'Conway';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/81939.jpg' WHERE prenom = 'John' AND nom = 'Cooney';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/126606.jpg' WHERE prenom = 'Max' AND nom = 'Deegan';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/113144.jpg' WHERE prenom = 'Ultan' AND nom = 'Dillane';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/131366.jpg' WHERE prenom = 'Caelan' AND nom = 'Doris';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/33232.jpg' WHERE prenom = 'Keith' AND nom = 'Earls';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/104651.jpg' WHERE prenom = 'Chris' AND nom = 'Farrell';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/100177.jpg' WHERE prenom = 'Tadhg' AND nom = 'Furlong';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/33268.jpg' WHERE prenom = 'Cian' AND nom = 'Healy';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/100010.jpg' WHERE prenom = 'Dave' AND nom = 'Heffernan';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/99509.jpg' WHERE prenom = 'Iain' AND nom = 'Henderson';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/106755.jpg' WHERE prenom = 'Robbie' AND nom = 'Henshaw';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/88438.jpg' WHERE prenom = 'Rob' AND nom = 'Herring';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/72808.jpg' WHERE prenom = 'David' AND nom = 'Kearney';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/131578.jpg' WHERE prenom = 'Ronan' AND nom = 'Kelleher';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/90371.jpg' WHERE prenom = 'David' AND nom = 'Kilcoyne';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/128410.jpg' WHERE prenom = 'Jordan' AND nom = 'Larmour';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/101424.jpg' WHERE prenom = 'Luke' AND nom = 'McGrath';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/81950.jpg' WHERE prenom = 'Jack' AND nom = 'McGrath';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/100638.jpg' WHERE prenom = 'Conor' AND nom = 'Murray';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/106493.jpg' WHERE prenom = 'Jack' AND nom = 'O''Donoghue';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/81946.jpg' WHERE prenom = 'Peter' AND nom = 'O''Mahony';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/132386.jpg' WHERE prenom = 'Tom' AND nom = 'O''Toole';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/122470.jpg' WHERE prenom = 'Andrew' AND nom = 'Porter';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/115242.jpg' WHERE prenom = 'Garry' AND nom = 'Ringrose';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/126401.jpg' WHERE prenom = 'James' AND nom = 'Ryan';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/15759.jpg' WHERE prenom = 'Jonathan' AND nom = 'Sexton';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/84664.jpg' WHERE prenom = 'CJ' AND nom = 'Stander';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/118708.jpg' WHERE prenom = 'Jacob' AND nom = 'Stockdale';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/15302.jpg' WHERE prenom = 'Devin' AND nom = 'Toner';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/126/250x250/105971.jpg' WHERE prenom = 'Josh' AND nom = 'van der Flier';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/105530.jpg' WHERE prenom = 'Tommaso' AND nom = 'Allan';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/123628.jpg' WHERE prenom = 'Mattia' AND nom = 'Bellini';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/82104.jpg' WHERE prenom = 'Tommaso' AND nom = 'Benvenuti';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/113916.jpg' WHERE prenom = 'Luca' AND nom = 'Bigi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/131344.jpg' WHERE prenom = 'Michelangelo' AND nom = 'Biondelli';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/105651.jpg' WHERE prenom = 'Giulio' AND nom = 'Bisegni';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/106205.jpg' WHERE prenom = 'Tommaso' AND nom = 'Boni';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/103109.jpg' WHERE prenom = 'Callum' AND nom = 'Braley';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/106504.jpg' WHERE prenom = 'Dean' AND nom = 'Budd';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/122905.jpg' WHERE prenom = 'Carlo' AND nom = 'Canna';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/131815.jpg' WHERE prenom = 'Niccolo' AND nom = 'Cannone';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/100511.jpg' WHERE prenom = 'Pietro' AND nom = 'Ceccarelli';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/116701.jpg' WHERE prenom = 'Oliviero' AND nom = 'Fabiani';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/131345.jpg' WHERE prenom = 'Danilo' AND nom = 'Fischetti';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/101816.jpg' WHERE prenom = 'Jayden' AND nom = 'Hayward';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/113906.jpg' WHERE prenom = 'Marco' AND nom = 'Lazzaroni';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/127641.jpg' WHERE prenom = 'Giovanni' AND nom = 'Licata';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/82116.jpg' WHERE prenom = 'Andrea' AND nom = 'Lovotti';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/122998.jpg' WHERE prenom = 'Johan' AND nom = 'Meyer';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/121491.jpg' WHERE prenom = 'Matteo' AND nom = 'Minozzi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/99836.jpg' WHERE prenom = 'Luca' AND nom = 'Morisi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/115494.jpg' WHERE prenom = 'Sebastian' AND nom = 'Negri';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/105608.jpg' WHERE prenom = 'Edoardo' AND nom = 'Padovani';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/95795.jpg' WHERE prenom = 'Guglielmo' AND nom = 'Palazzani';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/119366.jpg' WHERE prenom = 'Jake' AND nom = 'Polledri';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/126147.jpg' WHERE prenom = 'Marco' AND nom = 'Riccioni';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/115496.jpg' WHERE prenom = 'Federico' AND nom = 'Ruzza';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/100181.jpg' WHERE prenom = 'Leonardo' AND nom = 'Sarto';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/33364.jpg' WHERE prenom = 'Alberto' AND nom = 'Sgarbi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/97797.jpg' WHERE prenom = 'Dave' AND nom = 'Sisi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/106125.jpg' WHERE prenom = 'Abraham' AND nom = 'Jurgen Steyn';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/ITAL1189.svg' WHERE prenom = 'Jimmy' AND nom = 'Tuivaiti';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/teams/logos/RUGBY969513/d/ITAL1189.svg' WHERE prenom = 'Marcello' AND nom = 'Violi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/122904.jpg' WHERE prenom = 'Federico' AND nom = 'Zani';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/9585.jpg' WHERE prenom = 'Alessandro' AND nom = 'Zanni';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/130/250x250/127075.jpg' WHERE prenom = 'Giosue' AND nom = 'Zilocchi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/115130.jpg' WHERE prenom = 'Josh' AND nom = 'Adams';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/107144.jpg' WHERE prenom = 'Jake' AND nom = 'Ball';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/115391.jpg' WHERE prenom = 'Adam' AND nom = 'Beard';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/62620.jpg' WHERE prenom = 'Dan' AND nom = 'Biggar';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/121901.jpg' WHERE prenom = 'Leon' AND nom = 'Brown';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/130367.jpg' WHERE prenom = 'Rhys' AND nom = 'Carre';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/62155.jpg' WHERE prenom = 'Gareth' AND nom = 'Davies';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/113058.jpg' WHERE prenom = 'Seb' AND nom = 'Davies';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/92185.jpg' WHERE prenom = 'Elliot' AND nom = 'Dee';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/114623.jpg' WHERE prenom = 'Ryan' AND nom = 'Elias';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/62164.jpg' WHERE prenom = 'Rob' AND nom = 'Evans';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/116700.jpg' WHERE prenom = 'Jarrod' AND nom = 'Evans';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/90373.jpg' WHERE prenom = 'Taulupe' AND nom = 'Faletau';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/33325.jpg' WHERE prenom = 'Leigh' AND nom = 'Halfpenny';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/62008.jpg' WHERE prenom = 'Cory' AND nom = 'Hill';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/94724.jpg' WHERE prenom = 'Jonah' AND nom = 'Holmes';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/90525.jpg' WHERE prenom = 'WillGriff' AND nom = 'John';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/5071.jpg' WHERE prenom = 'Alun' AND nom = 'Wyn Jones';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/100041.jpg' WHERE prenom = 'Wyn' AND nom = 'Jones';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/123371.jpg' WHERE prenom = 'Owen' AND nom = 'Lane';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/97327.jpg' WHERE prenom = 'Dillon' AND nom = 'Lewis';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/127937.jpg' WHERE prenom = 'Johnny' AND nom = 'McNicholl';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/90612.jpg' WHERE prenom = 'Ross' AND nom = 'Moriarty';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/82067.jpg' WHERE prenom = 'Josh' AND nom = 'Navidi';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/80371.jpg' WHERE prenom = 'George' AND nom = 'North';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/16608.jpg' WHERE prenom = 'Ken' AND nom = 'Owens';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/120153.jpg' WHERE prenom = 'Hadleigh' AND nom = 'Parkes';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/137590.jpg' WHERE prenom = 'Louis' AND nom = 'Rees-Zammit';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/98731.jpg' WHERE prenom = 'Will' AND nom = 'Rowlands';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/83909.jpg' WHERE prenom = 'Aaron' AND nom = 'Shingler';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/62616.jpg' WHERE prenom = 'Justin' AND nom = 'Tipuric';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/108799.jpg' WHERE prenom = 'Nick' AND nom = 'Tompkins';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/134208.jpg' WHERE prenom = 'Aaron' AND nom = 'Wainwright';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/121512.jpg' WHERE prenom = 'Owen' AND nom = 'Watkin';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/33336.jpg' WHERE prenom = 'Rhys' AND nom = 'Webb';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/62296.jpg' WHERE prenom = 'Owen' AND nom = 'Williams';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/98574.jpg' WHERE prenom = 'Liam' AND nom = 'Williams';
UPDATE Joueur SET photo = 'https://cdn.soticservers.net/tools/images/players/photos/2019/sixnations/128/250x250/110814.jpg' WHERE prenom = 'Tomos' AND nom = 'Williams';


INSERT INTO Score(essai, transformation, penalite, numEq) VALUES
('5','4','3','6'), -- Pays de Galles/ Italie
('0','0','0','5'),
('1','1','4','4'), --Irlande/Ecosse
('0','0','4','2'),
('3','3','1','3'), -- France/Angleterre
('2','2','1','1'),

('4','2','0','4'), -- Irlande/ Pays de Galles
('2','2','0','6'),
('0','0','2','2'), -- Ecosse / Angleterre
('1','1','2','1'),
('5','2','2','3'), -- France / Italie
('3','2','1','5'),

('0','0','0','5'), -- Italie / Ecosse
('3','1','0','2'),
('2','2','3','6'), -- Pays de Galles / France
('3','3','2','3'),
('3','3','1','1'), -- Angleterre / Irlande
('2','1','0','4'),

(NULL, NULL, NULL,'4'), -- Irlande / Italie
(NULL, NULL, NULL,'5'),
('3','3','4','1'), -- Angleterre/Pays de Galles
('3','3','3','6'),
('3','2','3','2'), -- Ecosse/France
('2','2','1','3'),

(NULL, NULL, NULL,'6'), -- Pays de Galles / Ecosse
(NULL, NULL, NULL,'2'),
(NULL, NULL, NULL,'5'), -- Italie / Angleterre
(NULL, NULL, NULL,'1'),
(NULL, NULL, NULL,'3'), -- France / Irlande
(NULL, NULL, NULL,'4');


INSERT INTO Lieu(nomStade,ville,pays) VALUES
('Aviva Stadium','Dublin','Irlande'), -- 1
('Murrayfield Stadium','Édimbourg','Écosse'), -- 2
('Principality Stadium','Cardiff','Pays de Galles'), -- 3
('Stadio olympico','Rome','Italie'), -- 4
('Stade de France','Paris','France'), -- 5
('Twickenham','Londres','Angleterre'); -- 6

INSERT INTO Etat(libelleEtat) VALUES ('Non joué'),('Joué'),('En cours'),('Reporté');

INSERT INTO eMatch(annee,debut,fin,idScore_EquipeA,idScore_EquipeB,idLieu,idEtat) VALUES
(2020,'2020-02-01 15:15:00','16:39:00',1,2,3,2), -- Pays de Galles/ Italie
(2020,'2020-02-01 15:45:00','17:05:00',3,4,1,2), -- irlande - Ecosse
(2020,'2020-02-02 16:00:00','17:21:00',5,6,5,2), -- France/Angleterre
-- Tour 2
(2020,'2020-02-08 15:15:00','16:39:00',7,8,1,2), -- Irlande/ Pays de Galles
(2020,'2020-02-08 15:45:00','17:05:00',9,10,2,2), -- Ecosse / Angleterre
(2020,'2020-02-09 16:00:00','17:20:00',11,12,5,2), -- France / Italie
-- Tour 3
(2020,'2020-02-22 15:15:00','16:37:00',13,14,4,2), -- Italie / Ecosse
(2020,'2020-02-22 15:45:00','17:05:00',15,16,3,2), -- Pays de Galles / France
(2020,'2020-02-23 16:00:00','17:21:00',17,18,6,2), -- Angleterre / Irlande
-- Tour 4
(2020,'2020-03-07 15:15:00',NULL,19,20,NULL,4), -- Irlande / Italie
(2020,'2020-02-07 16:00:00','17:52:00',21,22,6,2), -- Angleterre/Pays de Galles
(2020,'2020-02-08 16:00:00','17:20:00',23,24,2,2), -- Ecosse/France
-- Tour 5
(2020, '2020-03-14 00:00:00', NULL,25,26,NULL,4), -- Pays de Galles / Ecosse
(2020, '2020-03-14 00:00:00', NULL,27,28,NULL,4), -- Italie / Angleterre
(2020, '2020-03-14 00:00:00', NULL,29,30,NULL,4); -- France / Irlande