CREATE TABLE Equipe(
   numEq SERIAL,
   pays VARCHAR(50) NOT NULL UNIQUE,
   photo VARCHAR(255),
   PRIMARY KEY(numEq)
);

CREATE TABLE Lieu(
   idLieu SERIAL,
   nomStade VARCHAR(50),
   ville VARCHAR(50),
   pays VARCHAR(50),
   PRIMARY KEY(idLieu)
);

CREATE TABLE Etat(
   idEtat SERIAL,
   libelleEtat VARCHAR(50),
   PRIMARY KEY(idEtat)
);

CREATE TABLE Utilisateur(
   idU SERIAL,
   nomU VARCHAR(50) NOT NULL UNIQUE,
   mdp VARCHAR(255) NOT NULL,
   admin BOOLEAN NOT NULL,
   pdp VARCHAR(50),
   inscription DATE NOT NULL DEFAULT CURRENT_DATE,
   PRIMARY KEY(idU)
);

CREATE TABLE Score(
   idScore SERIAL,
   essai INT,
   transformation INT,
   penalite INT,
   numEq INT NOT NULL,
   PRIMARY KEY(idScore),
   FOREIGN KEY(numEq) REFERENCES Equipe(numEq)
);

CREATE TABLE Joueur(
   licence SERIAL,
   nom VARCHAR(50) NOT NULL,
   prenom VARCHAR(50) NOT NULL,
   photo VARCHAR(255),
   naissance DATE,
   taille REAL,
   poids REAL,
   numEq INT,
   PRIMARY KEY(licence),
   FOREIGN KEY(numEq) REFERENCES Equipe(numEq)
);

CREATE TABLE eMatch(
   idMatch SERIAL,
   annee INT,
   debut TIMESTAMP,
   fin TIME,
   idLieu INT,
   idEtat INT NOT NULL,
   idScore_EquipeA INT NOT NULL,
   idScore_EquipeB INT NOT NULL,
   PRIMARY KEY(idMatch),
   FOREIGN KEY(idLieu) REFERENCES Lieu(idLieu),
   FOREIGN KEY(idEtat) REFERENCES Etat(idEtat),
   FOREIGN KEY(idScore_EquipeA) REFERENCES Score(idScore) ON DELETE CASCADE,
   FOREIGN KEY(idScore_EquipeB) REFERENCES Score(idScore) ON DELETE CASCADE
);

CREATE TABLE Commenter(
   idMatch INT,
   idU INT,
   texte TEXT NOT NULL,
   dateAjout TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
   PRIMARY KEY(idMatch, idU),
   FOREIGN KEY(idMatch) REFERENCES eMatch(idMatch),
   FOREIGN KEY(idU) REFERENCES Utilisateur(idU)
);

CREATE TABLE Parier(
   idU INT,
   idScore INT,
   idMatch INT,
   datePari TIMESTAMP NOT NULL default CURRENT_TIMESTAMP,
   PRIMARY KEY(idU, idScore, idMatch),
   FOREIGN KEY(idU) REFERENCES Utilisateur(idU),
   FOREIGN KEY(idScore) REFERENCES Score(idScore) ON DELETE CASCADE,
   FOREIGN KEY(idMatch) REFERENCES eMatch(idMatch)
);

CREATE OR REPLACE FUNCTION idScore_is_linked() -- vérifie que le score n'est attribué à rien avant d'autoriser l'insertion/modification
  RETURNS TRIGGER
 LANGUAGE plpgsql
  AS $sil$
DECLARE i INTEGER;
BEGIN
  IF (TG_TABLE_NAME = 'Parier') THEN
   IF EXISTS (SELECT FROM eMatch e, Parier p WHERE p.idScore = NEW.idScore OR e.idScore_EquipeA = NEW.idScore OR e.idScore_EquipeB = NEW.idScore) THEN
      RAISE EXCEPTION '% is already linked to another entity', NEW.idScore;
   END IF;
   SELECT COUNT(*) into i FROM Parier p WHERE p.idU = NEW.idU AND p.idMatch = NEW.idMatch;
   IF i >= 2 THEN
      RAISE EXCEPTION 'The Match % is already bet by %',NEW.idMatch,NEW.idU;
   END IF;
  ELSIF (TG_TABLE_NAME = 'eMatch') THEN
   IF EXISTS (SELECT FROM eMatch e, Parier p WHERE p.idScore = NEW.idScore_EquipeA OR e.idScore_EquipeA = NEW.idScore_EquipeA OR e.idScore_EquipeB = NEW.idScore_EquipeA
               OR p.idScore = NEW.idScore_EquipeB OR e.idScore_EquipeA = NEW.idScore_EquipeB OR e.idScore_EquipeB = NEW.idScore_EquipeB ) THEN
      RAISE EXCEPTION '% or % is/are already linked to another entity', NEW.idScore_EquipeA, NEW.idScore_EquipeB;
   END IF;
  END IF;
  RETURN NEW;
END
$sil$;

CREATE TRIGGER trg_parier_idscore_not_linked
   BEFORE UPDATE OR INSERT ON Parier 
   FOR EACH ROW
   EXECUTE FUNCTION idScore_is_linked();

CREATE TRIGGER trg_ematch_idscore_not_linked
   BEFORE UPDATE OR INSERT ON eMatch 
   FOR EACH ROW
   EXECUTE FUNCTION idScore_is_linked();